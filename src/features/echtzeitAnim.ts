import { AnimationAction, AnimationClip, AnimationMixer, LoopOnce, LoopRepeat } from "three";
import { ModelScene } from "../three-components/ModelScene";


interface IWAnim {
    name: string,
    weight: number,
    loop: number
}

interface IAnimWeightController {
    [key: string]: {
        action: AnimationAction,
        animClip: AnimationClip,
        // wCurrent: number, // current weight
        wTarget: number, // target weight
        loop: number,
        progress: number,
        needsUpdate:boolean
    }
}

interface INewData {
    [key: string]: IWAnim
}



export class EchtzeitAnim {
    //actions: Array<AnimationAction> = [];
    animWeightController: IAnimWeightController = {};
    scene;
    mixer: AnimationMixer | null = null;
    animNeedsChange: Array<string> = [];
    hasChanged: boolean = false;
    lerp;
    easing;

    //@ts-ignore
    constructor(scene: ModelScene, lerp, easing) {
        this.scene = scene;
        this.lerp = lerp;
        this.easing = easing;
    }


    // rest anim paused and weight 1

    initialiseAnimHandler() {
        this.mixer = this.scene['mixer'];
        //this.actions.length = 0;

        this.animNeedsChange.length = 0;

        // Collect all actions for each animClip
        this.scene.animations.forEach((animClip: AnimationClip) => {

            if (!this.mixer) return

            let action = this.mixer.existingAction(animClip);
            if (!action) {
                action = this.mixer.clipAction(animClip);
                // console.info("Creating a new action")
            }
            const name = action.getClip().name;


            this.animWeightController[name] = {
                action: action,
                animClip: animClip,
                // wCurrent: 0,
                wTarget: 0,
                loop: 0,
                progress: 0,
                needsUpdate:false
            }

            if (name.endsWith("@rest")) {
                // this.animWeightController[name].wCurrent = 1.0;
                this.animWeightController[name].wTarget = 1.0;

                this.animWeightController[name].loop = 0;
            }

            if (name.endsWith("@init")) {
                
                this.animWeightController[name].wTarget = 1.0;

                this.animWeightController[name].loop = 1;
            }

        })

        // console.info("animWeightController")
        // console.info(this.animWeightController)

        // Turn all animations on and set weights
        Object.keys(this.animWeightController).forEach(key => {

            const animCon = this.animWeightController[key];

            // Set the initial weight
            this.setWeight(animCon.action, animCon.wTarget);

            animCon.action.timeScale = 1;

            // see:
            // https://threejs.org/docs/?q=Action#api/en/animation/AnimationAction.clampWhenFinished
            animCon.action.clampWhenFinished = true;


            if (animCon.loop === 0) {
                animCon.action.setLoop(LoopRepeat, Infinity);
            } else if (animCon.loop === 1) {
                animCon.action.setLoop(LoopOnce, 1);
            } else {
                animCon.action.setLoop(LoopRepeat, animCon.loop);
            }



            if (animCon.wTarget === 0) {

                // from the docs about reset()
                // This method sets paused to false, enabled to true, time to 0, interrupts any scheduled fading and warping, and removes the internal loop count and scheduling for delayed starting.

                // Note: .reset is always called by stop, but .reset doesn’t call .stop itself. This means: If you want both, resetting and stopping, don’t call .reset; call .stop instead. 


                animCon.action.reset();

            } else {
                // animCon.action.reset();
                // animCon.action.paused = false;
                // animCon.action.enabled = true;



                animCon.action.play();
            }



            // console.info("Anim weight for ", key, " set to ", animCon.wTarget)
        })
        this.mixer.stopAllAction();
        this.scene.updateAnimation(0);


    }

    //@ts-ignore
    setWeight(action, weight) {
        action.enabled = true;
        action.setEffectiveTimeScale(1);
        action.setEffectiveWeight(Number(weight));
    }


    handleNewWeights(wAnims: Array<IWAnim>) {


        this.animNeedsChange.length = 0;


        // For better access, we first create an object out of the wAnims data:

        const newData: INewData = {} as INewData;

        for (let i = 0; i < wAnims.length; i++) {
            newData[wAnims[i].name] = wAnims[i]
        }


        // Now figure out what needs to change:

        Object.keys(this.animWeightController).forEach(anmName => {


            /*
            
            Figure out which case applies:
            delta means, there is a weight difference between current and target. If
            this is the case, .progress will be set to 0, to trigger a change.
            
            w0  noDelta noLoopChange -> do nothing
            w!0 noDelta noLoopChange -> do nothing

            w0  noDelta loopChange   -> do nothing
            w!0 noDelta loopChange   -> weightTo0AddDelta (will then "be w0 delta loopChange" )

            w0  delta   noLoopChange -> changeLoopAndWeight
            w!0 delta   noLoopChange -> changeWeight

            w0  delta   loopChange   -> changeLoopAndWeight
            w!0 delta   loopChange   -> weightTo0AddDelta (will then be "w0 delta loopChange" )


            */


            const currentWeight = this.animWeightController[anmName].action.getEffectiveWeight();
            const currentloop = this.animWeightController[anmName].action.loop;
            const currentRepetitions = this.animWeightController[anmName].action.repetitions;


            // Generally, all animation that has its weight at 0
            // Or is only running once
            // should be reset:
            if (currentWeight === 0 ){
                // console.info(anmName," weight is 0, reset")
                this.animWeightController[anmName].action.time = 0;
                this.animWeightController[anmName].action.timeScale = 1;
                this.animWeightController[anmName].action.paused = true;
                // this.animWeightController[anmName].action.stop();
                // this.animWeightController[anmName].action.enabled = false;
            }


            // Animation that is set to loopOnce should be restarted:
            if (currentloop === LoopOnce ){
                // console.info(anmName," weight is 0, reset")
                this.animWeightController[anmName].action.time = 0;
                this.animWeightController[anmName].action.timeScale = 1;

                this.animWeightController[anmName].action.paused = false;
                this.animWeightController[anmName].action.reset().play();
            }


            // If there is no new anim data, we want this anim to
            // fade out or stay at weight 0.
            // If we have  new anim data,
            // we check what changes are required.

            // Handle NO NEW DATA first: 

            if (!newData[anmName]) {

                // No new data for this anim.
                // If its weight is 0, no updates during rendering are required.
                // If it is not 0, we want to fade it out:

                if (currentWeight === 0) {
                    // No change is required:
                    this.animWeightController[anmName].action.stop();
                    this.animWeightController[anmName].action.time = 0;
                    this.animWeightController[anmName].action.timeScale = 1;
                    this.animWeightController[anmName].action.enabled = false;
                    this.animWeightController[anmName].progress = 1;
                    this.animWeightController[anmName].needsUpdate = false;
                    return; // skips to next iteration in forEach
                }


                this.animWeightController[anmName].progress = 0;
                this.animWeightController[anmName].wTarget = 0;
                this.animWeightController[anmName].needsUpdate = true;
                // console.info(anmName, " needs to be faded Out, current weight: ", currentWeight)
                return;

            }

            // Handle NEW DATA:

            const targetWeight = Number(newData[anmName].weight);
            const targetloop = Number(newData[anmName].loop);
            let weightChangeRequired = false;
            let loopChangeRequired = false;

            // We check if a weight change and/or a loop change is required.
            // If one of them is true, .progress will be set to 0.
            // In case of weight change, targetWeight will be set. 
            // In case of loop change, we brute force set weight to 0 and change
            // the loop mode.

            // Weight change required?
            if (currentWeight != targetWeight) {
                weightChangeRequired = true;
            }

            // Loop change requred?
            switch (targetloop) {
                case 0:
                    // Target is loop infinite
                    if (currentloop != LoopRepeat) {
                        // change required
                        loopChangeRequired = true;
                    }
                    break;
                case 1:
                    // Target is loop once
                    if (currentloop != LoopOnce) {
                        // change required
                        loopChangeRequired = true;
                    }
                    break;
                default:
                    // Target is loop x amount of times
                    if (currentloop != LoopRepeat || targetloop != currentRepetitions) {
                        // change required
                        loopChangeRequired = true;
                    }
            }

            // Initialise to no changes required
            this.animWeightController[anmName].needsUpdate = false;
            this.animWeightController[anmName].progress = 1;


            if (weightChangeRequired && !loopChangeRequired) {
                this.animWeightController[anmName].progress = 0;
                this.animWeightController[anmName].needsUpdate = true;
                this.animWeightController[anmName].wTarget = targetWeight;

                // console.info(anmName, "weight change YES, loop change NO")
            }

            if (!weightChangeRequired && loopChangeRequired) {
                // Trigger the change:
                this.animWeightController[anmName].progress = 0;
                this.animWeightController[anmName].needsUpdate = true;

                // Only Loop change is required:
                // we brute force set weight to 0 and change
                // the loop mode.
                // But first we set TargetWeight to currentweight, so it animates
                // back to its original weight.

                this.animWeightController[anmName].wTarget = currentWeight;
                this.setWeight(this.animWeightController[anmName].action, 0);

                const animCon = this.animWeightController[anmName];

                if (targetloop === 0) {
                    animCon.action.setLoop(LoopRepeat, Infinity);
                } else if (targetloop === 1) {
                    animCon.action.setLoop(LoopOnce, 1);
                } else {
                    animCon.action.setLoop(LoopRepeat, targetloop);
                }

                // console.info(anmName, "weight change NO, loop change YES")

            }

            if (weightChangeRequired && loopChangeRequired) {
                // Trigger the change in the update loop:
                this.animWeightController[anmName].progress = 0;
                this.animWeightController[anmName].needsUpdate = true;
                this.animWeightController[anmName].wTarget = targetWeight;

                this.setWeight(this.animWeightController[anmName].action, 0);

                const animCon = this.animWeightController[anmName];

                if (targetloop === 0) {
                    animCon.action.setLoop(LoopRepeat, Infinity);
                } else if (targetloop === 1) {
                    animCon.action.setLoop(LoopOnce, 1);
                } else {
                    animCon.action.setLoop(LoopRepeat, targetloop);
                }

                // console.info(anmName, "weight change YES, loop change YES")
            }

        })

        // Tell the update loop, that changes are required:
        this.hasChanged = true;

        // console.info("Changes required:")
        // console.info(this.animWeightController)
    }



    update(delta: number) {
        delta;

        if (this.hasChanged) {

            // Initialise weights as matching. As long as changes are made to the weights,
            // this will be set to false. If it is still true at the end of the update loop, 
            // .hasChanged will be set to false:
            let weightsMatch = true;

            Object.keys(this.animWeightController).forEach(anm => {


                const animCon = this.animWeightController[anm];
                
                
                // if(!animCon.needsUpdate)return;
                // TODO remove .needsUpdate?
                
                const currentWeight = animCon.action.getEffectiveWeight();
                const targetWeight = animCon.wTarget;

                const duration = 100

                
                
                // Only operate on anims if .progress < 1
                if(animCon.progress >= 1) return;

                // If an animation needs to be updated and its weight is 0,
                // before we animate its weight, we need to make sure it starts from the beginning:
                if (currentWeight === 0) {
                    animCon.action.enabled = true;
                    animCon.action.time = 0;
                    animCon.action.timeScale = 1;
                    animCon.action.paused = false;
                    animCon.action.reset().play();
                }

                // Do the actual weight animation:
                // Handle new weight and progress
                const w = this.lerp(currentWeight, targetWeight, this.easing("easeInOutSine", animCon.progress));

                this.setWeight(this.animWeightController[anm].action, w);

                animCon.progress = animCon.progress + (1 / duration);


                // If weight reached 0 and no more changes are required,
                // we need to  disable this animation.
                if (currentWeight === 0 && animCon.progress >= 1) {
                    // This might not happen, if another shot is triggered first!
                    // console.info(anm, " Resetting anim ", animCon.action.weight)
                    animCon.action.time = 0;
                    animCon.action.timeScale = 1;
                    animCon.action.stop();
                    animCon.action.enabled = false;
                }

                // Sets weights match to false until nothing falls through anymore
                weightsMatch = false;

            })

            this.scene.updateAnimation(0);

            // When all weights match, no more change is required
            if (weightsMatch) {
                this.hasChanged = false;

                // console.info("All weights match!")
            }

        }
    }

}