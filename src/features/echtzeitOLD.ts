// @ts-nocheck
import { Vector3, AxesHelper, Object3D, Quaternion, SphereGeometry, MeshBasicMaterial, Mesh, BoxGeometry, Material, AdditiveBlending, FrontSide, BackSide, DoubleSide, NoBlending, NormalBlending, SubtractiveBlending, MultiplyBlending } from 'three';
import type { ModelScene } from '../three-components/ModelScene';


interface IComponentLibrary {
    [key: string]: Object3D
}


interface IObjectToMaterialMapping {
    [key: string]: string;
}

interface ITweenItem {
    valueStart: number,
    valueEnd: number,
    // valueDelta: number,
    duration: number,
    progress: number,
    ease: string
}

enum ETween {
    exposure = "exposure"
}

type ITweenStore = {
    [key in ETween]?: ITweenItem
}

interface IuvOffsetMaterialItem {
    material: Material,
    sourceVec: Vector3,

}
interface IuvOffsetMaterials {
    [key: string]: IuvOffsetMaterialItem
}

enum EObjectFadeType {
    fadeIn = "fadeIn",
    fadeOut = "fadeOut",
    fadeCustom = "fadeCustom",
    fadeNone = "fadeNone" // use the original material
}


interface IFadeMaterials {
    [key: string]: {
        //[key in EObjectFadeType]: Material
        fadeIn: Material,
        fadeOut: Material,
        fadeCustom: Material,
        fadeNone: Material,
        fadeCustomAmount: number
    }
}
interface IObjectNameToFadeMapping {
    [key: string]: EObjectFadeType
}

export default class Echtzeit {
    scene: ModelScene;
    currentShot: string = "";
    newShot: boolean = false;
    utilVec = new Vector3();
    componentLib: IComponentLibrary = {};
    rctNodes: Array<Object3D> = []; // raycastTarget nodes. Nodes that can be clicked.
    ucxNodes: Array<Object3D> = [];
    hookNodes: Array<Object3D> = [];
    hookVisNodes: Array<Object3D> = [];
    fadeMaterials: IFadeMaterials = {};
    objectToMaterialMapping: IObjectToMaterialMapping = {};
    fadeCounter: number = 0;
    fadeDuration: number = 40;
    fadeInProgress: boolean = false;
    objectNameToFadeMapping: IObjectNameToFadeMapping = {};
    isFading: Array<string> = [];
    ezTweenStore: ITweenStore = {};
    collections = {};
    materials = new Set();
    enableObjectNames: Set<unknown> = new Set();
    enableNodes: Array<Object3D> = []; // THREE.Object3D Scene nodes that can be enabled/disabled from a shot
    uvOffsetMaterials: IuvOffsetMaterials = {};
    dev = false;
    shotData
    shotID
    ucxMeshMapping
    gltfLoadData
    collections;
    element;
    isLoading = false;
    progressScreen;
    progressBarWrapper;
    progressBar;
    progressAmount;
    lineRenderElements;
    currentModel;
    currentOrbit;
    nextOrbit;
    currentCameraTarget;
    nextCameraTarget
    animGraphCurrent

    animGraphToPlay


    constructor(scene: ModelScene, element, dev = false) { 
        this.scene = scene;
        this.element = element;
        this.dev = dev;
        this.scene.exposure = 0;

        this.element.addEventListener('camera-change', this.handleCameraInteraction.bind(this));
        this.element.addEventListener('progress', this.updateProgressbar);
        this.element.addEventListener('finished', this.handleAnimationFinished);
        console.info("Echtzeit3D")

        const initEvent = new Event('echtzeit');
        this.element.dispatchEvent(initEvent);
    }


    posterAndProgressbar() {
        // Add the progressbar
        this.progressScreen = document.createElement('div');
        this.progressScreen.classList.add("__pscreen__");

        this.progressBarWrapper = document.createElement('div');
        this.progressBarWrapper.classList.add("__pbar__");
        this.progressBar = document.createElement('div');
        this.progressBarWrapper.appendChild(this.progressBar);

        this.progressAmount = document.createElement('span');
        this.progressAmount.innerText = "0%";

        this.progressScreen.appendChild(this.progressBarWrapper);
        this.progressScreen.appendChild(this.progressAmount);

        this.progressScreen.slot = `progress-bar`;

        this.element.appendChild(this.progressScreen);
    }

    updateProgressbar = (event) => {
        if (!this.progressScreen || this.isLoading) {
            return;
        }
        this.progressBar.style.width = event.detail.totalProgress * 100 + "%";
        this.progressAmount.innerText = Math.trunc(event.detail.totalProgress * 100) + "%";

    }

    removeAllChildNodes() {
        // Remove all annotations and hotspots
        const annot = this.element.querySelectorAll('[slot^="hotspot"]');
        const svg = this.element.querySelectorAll('svg');
        if (annot.length) {
            for (const el of annot) {
                el.parentNode.removeChild(el);
            }
        }

        if (svg.length) {
            for (const el of svg) {
                console.info("Removing SVG: ")
                console.log(el);
                el.parentNode.removeChild(el);
            }
        }

        this.lineRenderElements.length = 0;
    }


    handle(shotData, gltfLoadData, collections, ucxMEshMapping,) {
        this.shotData = shotData;
        this.ucxMeshMapping = ucxMEshMapping;
        this.gltfLoadData = gltfLoadData;
        this.collections = collections;

        if (this.shotData.load) {
            const clonedData = JSON.parse(JSON.stringify(this.shotData));
            this.loadModel(clonedData);
            return;
        }
        // If no model loaded and no .load data in shot, do nothing
        if (!get(currentModel)) {
            // No model loaded. TODO sollte trotzdem den modelviewer starten.
            console.info("No model loaded and no model to load.")
            return;
        }
        this.applyShot();
    }

    async loadModel(s) {
        console.info("Load model!")

        const loadFinishedHandler = () => {
            console.log('New model loaded');
            this.currentModel = loadInfo.model;

            this.isLoading = false;

            if (this.progressScreen) {
                this.progressScreen.style.width = "0rem";
            }

            this.currentOrbit = this.element.getCameraOrbit();
            this.currentCameraTarget = this.element.getCameraTarget();

            this.clearAndCollect();

            if (this.dev) {
                this.sceneHierarchy();
                this.visualizeNodes();
            }
            // Apply shot with inital anim
            this.applyShot(true);


        };


        // Shot.load is array for additive loading. For now, we can only load one model. Therefore
        // we only load the first one.
        if (!s?.load?.length) {
            console.info("No model to load");
            this.applyShot();
            return
        }
        if (!this.gltfLoadData) {
            console.info("No gltfLoadData");
            return
        }

        const loadInfo = this.gltfLoadData[s.load[0]];


        if (!loadInfo) {
            console.info("Model to load is not in list")
            this.applyShot();
            return;
        }

        if (this.currentModel === loadInfo.model) {
            console.log('Model already loaded');
            this.applyShot();
            return;
        }
        this.isLoading = true;

        this.removeAllChildNodes();

        // set the new source
        this.element.src = loadInfo.model.replace(".glb", "_merged.glb");
        this.element.minCameraOrbit = `auto auto 0.1m`;
        this.element.maxCameraOrbit = `auto auto auto`;
        this.element.maxFieldOfView = '150deg'; //data.fov;
        this.element.minFieldOfView = '10deg';

        this.element.removeEventListener('load', loadFinishedHandler.bind(this));
        this.element.addEventListener('load', loadFinishedHandler.bind(this));

    }

    /**
     * Check if the current shot is new.
     * @param shotID string
     */
    isShotNew(shotID: string) {
        if (shotID != this.currentShot) {
            this.newShot = true;
            this.currentShot = shotID;
        } else {
            this.newShot = false;
        }
    }

    sceneHierarchy() {
        const scn = this.scene
        this.scene.traverse(function (obj: Object3D) {

            var s = '|___';

            var obj2: Object3D = obj;

            while (obj2 !== scn) {

                s = '\t' + s;
                //@ts-ignore
                obj2 = obj2.parent;

            }
            //@ts-ignore
            if (obj.material) {
                //@ts-ignore
                console.info(s + obj.name + ' <' + obj.type + '>' + '  Mat: ' + obj.material.name);
            } else {
                console.info(s + obj.name + ' <' + obj.type + '>')
            }

        });

    }

    visualizeSceneTarget() {

        const axesHelper = new AxesHelper(5);

        //axesHelper.position.copy(this.scene.target.position);

        this.scene.target.add(axesHelper);
    }

    buildObjectSet() {
        // Build set with all objectnames from collections data.
        // We need a Set because Objects can be in several collections. 

        const objectSet = new Set();
        if (!this.collections) {
            return;
        }
        Object.keys(this.collections).forEach((gltfName) => {
            //@ts-ignore
            Object.keys(this.collections[gltfName]).forEach((collectionName) => {
                //@ts-ignore
                this.collections[gltfName][collectionName].forEach((o) => {
                    objectSet.add(o);
                });
            });
        });

        this.enableObjectNames = objectSet;
    }

    clearAndCollect() {

        // Clear
        for (const prop of Object.getOwnPropertyNames(this.componentLib)) {
            delete this.componentLib[prop];
        }

        this.rctNodes.length = 0;
        this.hookNodes.length = 0;
        this.hookVisNodes.length = 0;

        this.enableObjectNames.clear();
        this.enableNodes.length = 0;
        this.materials.clear();

        // Clear Fading stuff
        for (var member in this.objectNameToFadeMapping) delete this.objectNameToFadeMapping[member];

        this.isFading.length = 0;
        for (var member in this.fadeMaterials) delete this.fadeMaterials[member];
        for (var member in this.objectToMaterialMapping) delete this.objectToMaterialMapping[member];

        this.buildObjectSet();

        // Collect enableNodes and UCX nodes:
        this.scene.traverse((n) => {

            if (n.name.startsWith('UCX_') || n.name.startsWith('UCXTARGET_') || n.name.startsWith('UCXLINE_')) {

                this.ucxNodes.push(n);

            } else {
                // Check if the node is in one of the collections
                if (this.enableObjectNames.has(n.name)) {
                    this.enableNodes.push(n);
                    this.objectNameToFadeMapping[n.name] = EObjectFadeType.fadeNone;

                    //@ts-ignore
                    if (n.material) {

                        // @ts-ignore
                        this.materials.add(n.material)


                        // Also collect into fadeMaterials.
                        //@ts-ignore
                        this.fadeMaterials[n.material.name] = {
                            //@ts-ignore
                            fadeNone: n.material, //if object fade is set to fadeNone, apply this.
                            //@ts-ignore
                            fadeIn: n.material.clone(),
                            //@ts-ignore
                            fadeOut: n.material.clone(),
                            //@ts-ignore
                            fadeCustom: n.material.clone(),
                            fadeCustomAmount: 0.5
                        }
                        //@ts-ignore
                        this.fadeMaterials[n.material.name].fadeIn.transparent = true;
                        //@ts-ignore
                        this.fadeMaterials[n.material.name].fadeOut.transparent = true;
                        //@ts-ignore
                        this.fadeMaterials[n.material.name].fadeCustom.transparent = true;
                    };


                }
            }
        });

        this.ucxNodes.forEach(n => n.visible = false)


        // collect RCT_ (raycastTarget)
        this.scene.traverse((n) => {

            if (n.name.startsWith('RCT_')) {
                this.rctNodes.push(n);

            }
        });
        //this.scene.rctNodes.forEach(n => n.visible = false)


        // collect HOOK_
        this.scene.traverse((n) => {

            if (n.name.startsWith('HOOK_')) {
                this.hookNodes.push(n);

            }
        });
        //this.scene.hookNodes.forEach(n => n.visible = false)



        // collect componentLibrary

        this.scene.traverse((n) => {

            if (n.name.startsWith('CMP_')) {
                // Register as component
                // hide

                this.componentLib[n.name] = n; // defined in ModelScene

                //console.info("Registered ", n.name, " in component library")
                n.visible = false
            }

        });
    }

    visualizeNodes() {

        let dir = new Vector3();
        let worldQuaternion = new Quaternion(); // create once and reuse it

        this.rctNodes.forEach(r => {
            const geometry = new BoxGeometry(1, 1, 1);
            const material = new MeshBasicMaterial({ color: 0xfacc15 });
            const rctGeo = new Mesh(geometry, material);
            r.add(rctGeo);
        })

        this.hookNodes.forEach(h => {

            h.getWorldQuaternion(worldQuaternion);
            dir.copy(h.up).applyQuaternion(worldQuaternion);


            const geometry = new SphereGeometry(1, 8, 8);
            const material = new MeshBasicMaterial({ color: 0xb91c1c });
            const hookGeo = new Mesh(geometry, material);
            h.add(hookGeo);

            this.hookVisNodes.push(hookGeo);

        })

    }


    handleCameraInteraction(event) {
        if (event.detail.source === 'user-interaction') {
            // Camera changed by user
            this.element.interpolationDecay = 50;
        } else {
            this.element.interpolationDecay = 200;
        }
    }

    handleAnimationFinished = (event) => {
        // console.info(this.modelviewer.animationName)
        // console.info("Loop count ", event.detail.count);

        // split animationname by @ to get rigname 
        // this.animGraphToPlay
        if (!this.element.animationName) {
            return;
        }
        const rigName = this.element.animationName.split('@')[0];
        this.handleAnimation(this.animGraphToPlay[rigName]);

    }




    applyShot(initialAnim = false) {
        console.info("Apply Shot");
        const clonedData = JSON.parse(JSON.stringify(this.shotData));
        const sData = clonedData;

        if (initialAnim) {
            // When model is loaded, apply materialTweaks and uvOffsets:
            if (sData?.materialTweaks) {
                console.info("MaterialTweaks")
                this.materialTweaks(sData.materialTweaks);
            }

            if (sData?.uvOffset) {
                this.uvOffset(sData.uvOffset);
            }
        }


        this.isShotNew(this.shotID);

        // set camera and env
        if (sData?.animation) {
            Object.keys(sData.animation).forEach(entityName => {
                const anim = sData?.animation[entityName];


                switch (anim.type) {
                    case 'camera':
                        this.currentOrbit = this.element.getCameraOrbit();
                        
                        const clonedData = JSON.parse(JSON.stringify(this.currentOrbit));
                        this.nextOrbit = clonedData;
                        this.currentCameraTarget = this.element.getCameraTarget();

                        this.nextCameraTarget =JSON.parse(JSON.stringify(this.currentCameraTarget));
                        // this.nextCameraTarget = cloneDeep(this.currentCameraTarget);
                        // azimuthal = theta
                        // polar = phi (phi is measured down from the top)
                        if (anim.keys[0]?.camera?.float?.yaw != null) {
                            this.nextOrbit.theta = this.degreesToRadians(anim.keys[0].camera.float.yaw);
                        }
                        if (anim.keys[0]?.camera?.float?.pitch != null) {
                            this.nextOrbit.phi = this.degreesToRadians(anim.keys[0].camera.float.pitch);
                        }
                        if (anim.keys[0]?.camera?.float?.distance != null) {
                            this.nextOrbit.radius = anim.keys[0].camera.float.distance;
                        }



                        if (anim.keys[0]?.camera?.pivotPoint != null) {


                            if (typeof anim.keys[0].camera.pivotPoint === "string") {
                                // find object in scene and get its position
                                const s = anim.keys[0].camera.pivotPoint;
                                console.info("Is string ", s)
                                const foundPosition = this.scene.echtzeit.worldPosFromObjectName(s)
                                if (foundPosition) {
                                    anim.keys[0].camera.pivotPoint = foundPosition;
                                }

                            }
                            if (anim.keys[0]?.camera?.pivotPoint?.x != null) {
                                this.nextCameraTarget.x = anim.keys[0].camera.pivotPoint.x;
                                this.nextCameraTarget.y = anim.keys[0].camera.pivotPoint.y;
                                this.nextCameraTarget.z = anim.keys[0].camera.pivotPoint.z;
                            }
                        }

                        if (anim.keys[0]?.camera?.fov != null) {
                            //if (anim.keys[0]?.camera?.minFov != null) {
                            //    this.modelviewer.minFieldOfView=anim.keys[0].camera.minFov.toString() + "deg";
                            //}
                            //if (anim.keys[0]?.camera?.maxFov != null) {
                            //    this.modelviewer.maxFieldOfView=anim.keys[0].camera.maxFov.toString() + "deg";
                            //}
                            this.element.fieldOfView = anim.keys[0].camera.fov.toString() + "deg";

                        }

                        // this.modelviewer.interpolationDecay = 50;
                        this.element.cameraOrbit = this.nextOrbit.toString();
                        this.element.cameraTarget = this.nextCameraTarget.toString();
                        break
                    case 'env':
                        console.info("Current exposure: ", this.element.exposure)
                        if (anim.keys[0]?.env?.skyboxIntensity != null) {
                            this.scene.echtzeit.exposure(anim.keys[0]?.env?.skyboxIntensity, anim.keys[0]?.duration ? anim.keys[0].duration : 80)
                        }
                        break;

                    default:
                        console.info("No Animation Handler for type ", anim.type)
                }
            })

        }

        // Handle visibilities. Will set enableNodes and fading objects
        this.handleVisibilities(sData, this.currentModel)


        // Check if animGraph in shot is already playing.
        // Compare
        let sameAnimGraph = false;
        if (
            (JSON.stringify(this.animGraphCurrent) === JSON.stringify(sData.animGraph))) {
            // console.info("Shot has same animGraph.")
            sameAnimGraph = true;

        }

        if (initialAnim) {

            // When model is loaded, run its first animation.

            // For each gltf in .load, add its first anim at start of animGraph Array.
            // There can be an existing animGraph, or not.

            if (sData?.load?.length) {
                sData.load.forEach(gltf => {
                    if (!Array.isArray(this.gltfLoadData[gltf].animation)) {
                        return;
                    }
                    const rigName = this.gltfLoadData[gltf].rigname;
                    const initAnimName = this.gltfLoadData[gltf].animation[0].name;

                    // Does shot have animGraph.rigname?
                    if (Array.isArray(sData.animGraph[rigName])) {
                        // True -> add init anim at start
                        sData.animGraph[rigName].unshift({ name: initAnimName });
                    } else {
                        // False -> add rigname:[{anim}]
                        if (!sData.animGraph) {
                            sData.animGraph = {}
                        }
                        sData.animGraph[rigName] = [{ name: initAnimName }]
                    }
                })
            }
        }

        if (sData?.animGraph && !sameAnimGraph) {

            this.animGraphCurrent = JSON.parse(JSON.stringify(sData.animGraph));
            this.animGraphToPlay =  JSON.parse(JSON.stringify(sData.animGraph));
            // Loop over rignames:
            Object.keys(this.animGraphToPlay).forEach(rigName => {
                const graph = this.animGraphToPlay[rigName];
                this.handleAnimation(graph);
            })
        }


        if (sData.background) {

            const container = document.getElementById("ezGradientContainer");


            if (!container) {
                return
            }

            // early return if gradients are the same.


            // Add new element, fade in
            const x = sData.background.X
            const y = sData.background.Y
            const cols = sData.background.colors.join(",")

            const newBG = document.createElement("div");

            // Class set in app.scss
            newBG.classList.add("ezBG");

            newBG.style.backgroundImage = `radial-gradient(circle at ${x}% ${y}%, ${cols}),url("./grain.jpg")`;
            newBG.style.width = "100%";
            newBG.style.height = "100%";
            newBG.style.animationDuration = "2s";
            newBG.style.animationName = "fadeIn";
            newBG.style.animationTimingFunction = "ease-out";
            container.appendChild(newBG);

            // Remove previousSibling when anim done
            newBG.addEventListener("animationend", () => { if (event.target.previousSibling) { event.target.previousSibling.remove() } });

            // add fadeIn class
            // newBG.classList.add("ezGradientFade");

        }

        if (!sData?.keepHotspots) {
            this.handleHotspots(sData);
        }

        if (initialAnim) {
            // Remove poster frame
            console.info("Dismiss poster")
            this.element.dismissPoster();
        }
        //await this.modelviewer.updateComplete;
        //this.modelviewer.play();
        this.scene.isDirty = true;
    }

    async handleAnimation(animGraph) {
        // handle no Array
        if (!Array.isArray(animGraph)) {
            console.info("animGraph not Array")
            return;
        }
        // handle no length
        if (!animGraph.length) {
            // console.info("End of animGraph")
            return;
        }


        // Play first in list and remove it from list.
        // PLAY ONCE and LOOP
        // Modelviewer might be too slow to catch the end of an animation before it loops.
        // Therefore we need to set every anim that is not the last in the graph to only play ONCE!

        const animToPlay = animGraph.shift();
        // Still entries left in animGraph?
        let loop = false;
        if (!animGraph.length) {
            loop = true;
        }

        this.element.animationName = animToPlay.name;
        if (animToPlay.speed != null) {
            this.element.timeScale = animToPlay.speed;
        }

        if (animToPlay.crossfade != null) {
            this.element.animationCrossfadeDuration = animToPlay.crossfade;
        }
        await this.element.updateComplete;

        if (loop) {
            this.element.play();
        } else {
            this.element.play({ repetitions: 1 });
        }
        console.info("Playing anim ", animToPlay.name, " as loop:", loop);
        return;
    };

    startSVGRenderLoop = () => {
        // If no lines to draw, return;
        if (!this.lineRenderElements.length) {
            console.info("SVG Renderloop stopped.")
            return;
        }
        // set x1, y1 and x2,y2 on line
        this.lineRenderElements.forEach((line) => {
            // get data start and end
            const start = this.element.queryHotspot('hotspot-' + line.dataset.start);
            const end = this.element.queryHotspot('hotspot-' + line.dataset.end);
            line.setAttribute('x1', start?.canvasPosition?.x);
            line.setAttribute('y1', start?.canvasPosition?.y);
            line.setAttribute('x2', end?.canvasPosition?.x);
            line.setAttribute('y2', end?.canvasPosition?.y);

        })
        requestAnimationFrame(this.startSVGRenderLoop);
    };

    onShotLeave() {

    }

    onShotEnter() {

    }

    worldPosFromObjectName(name: string) {
        const found = this.scene.getObjectByName(name);

        if (found) {

            found.getWorldPosition(this.utilVec);
            // console.info("utilvec WORLDSPACE:")
            // console.info(this.utilVec.x, this.utilVec.y, this.utilVec.z)


            // convert to scene.target localspace 
            this.scene.target.worldToLocal(this.utilVec);
            // console.info("utilvec scene.target LOCALSPACE:")
            // console.info(this.utilVec.x, this.utilVec.y, this.utilVec.z)


            // const result = this.utilVec.clone();


            return this.utilVec;


        } else {

            return null;

        }

    }

    addInstance(hook: any | undefined) {
        // Test: if complib, add first
        if (!hook) {
            return
        }
        if (Object.keys(this.componentLib).length) {
            const first = Object.keys(this.componentLib)[0];
            const firstClone = this.componentLib[first].clone();
            firstClone.visible = true;
            hook.object.add(firstClone);
        }
    }

    collectionsToObjects(collectionArray: Array<string>, currentModel: string) {
        const objectSet = new Set()

        //@ts-ignore
        collectionArray.forEach(cName => {
            //@ts-ignore
            if (cName in this.collections[currentModel]) {

                //@ts-ignore
                this.collections[currentModel][cName].forEach(o => {
                    objectSet.add(o)

                })
            }
        })

        return [...objectSet];
    }

    /**
     * When fading, objects get a fading material assigned (transparent:true, opacity:#)
     * When fade is done, switch objects back to the original material.
     */
    resetFadeMaterials() {

        this.enableNodes.forEach(obj => {
            // Set visibility
            // Objects that have a fadingState of fadeOut
            // will get their visbility set to false.
            switch (this.objectNameToFadeMapping[obj.name]) {
                case (EObjectFadeType.fadeOut):
                    obj.visible = false;
                    break;
                default: null;
            }

            // Reset Material unless it is fadeCustom
            if (this.objectNameToFadeMapping[obj.name] !== EObjectFadeType.fadeCustom) {
                this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeNone;

                //@ts-ignore
                if (obj.material) {
                    //@ts-ignore
                    obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeNone];
                }
            }


            // Reset fadeIn/fadeOut opacities
            Object.keys(this.fadeMaterials).forEach((m) => {
                //@ts-ignore
                this.fadeMaterials[m].fadeIn.opacity = 0;
                //@ts-ignore
                this.fadeMaterials[m].fadeOut.opacity = 1;
            })
        })

    }

    handleVisibilities(sData: object, currentModel: string) {
        //@ts-ignore
        if (!this.collections || !this.collections[currentModel]) {
            return
        }

        // Do not fade if we are in the same shot!
        if (!this.newShot) {
            return
        }


        // We will process visibilities first, so objects can fade in and out.
        // If objects are set via fadeCollection, they will be handled 
        // right after, so they overwrite whatever the enabledHandler is doing.

        // handle object visibilities. 
        // This will sort objects by their fadeState (in, out, custom, none)
        this.enabledHandler(sData, currentModel);

        // handle fade
        // this.fadeHandler(sData, currentModel);


        // Start visibility counter. Will be used to drive fadeIn and Out
        this.fadeCounter = 0;
        this.fadeInProgress = true;

    }

    /**
     * Handles visibilities and fading objects
     * @param sData 
     * @param currentModel 
     * @returns 
     */
    enabledHandler(sData: object, currentModel: string) {
        //@ts-ignore
        if (!Array.isArray(sData.setEnabled) || sData.enabledState == null) {
            return
        }

        //@ts-ignore
        if (sData.fadeDuration != null) {
            //@ts-ignore
            this.fadeDuration = sData.fadeDuration;
        }

        // Convert list of collections to list of objects
        //@ts-ignore
        const listOfObjects = this.collectionsToObjects(sData.setEnabled, currentModel) as Array<string>;
        let fadeListOfObjects: Array<string> = []
        let filteredListOfObjects: Array<string> = [];

        // Remove objects in setEnabled list  that are in fadeCollection
        //@ts-ignore
        if (Array.isArray(sData.fadeCollections || sData.fadeAmount != null)) {
            //@ts-ignore
            fadeListOfObjects = this.collectionsToObjects(sData.fadeCollections, currentModel);
            filteredListOfObjects = listOfObjects.filter(x => !fadeListOfObjects.includes(x));
        } else {
            filteredListOfObjects = listOfObjects
        }

        this.enableNodes.forEach(obj => {
            //@ts-ignore
            switch (sData.enabledState) {
                case true:
                    // Handle ALL objects in the scene.
                    if (filteredListOfObjects.includes(obj.name)) {
                        // Object needs to be visible
                        if (obj.visible) {
                            // do nothing / apply original material and keep visibility
                            this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeNone;
                            //@ts-ignore
                            if (obj.material) {
                                //@ts-ignore
                                obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeNone];
                            }
                            obj.visible = true;
                        } else {
                            this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeIn;
                            //@ts-ignore
                            if (obj.material) {
                                // console.info("this.fadeMaterials[obj.material.name][EObjectFadeType.fadeIn]")
                                //@ts-ignore
                                // console.info(this.fadeMaterials[obj.material.name][EObjectFadeType.fadeIn])
                                //@ts-ignore
                                obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeIn];
                            }
                            // Set it visible now, so we can see the fadeIn
                            obj.visible = true;
                        }
                    } else {
                        // Object needs to be hidden
                        if (obj.visible) {
                            // fade out
                            this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeOut;
                            //@ts-ignore
                            if (obj.material) {
                                //@ts-ignore
                                obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeOut];
                                // For objects that have no material, they will be set visible after the fade.
                            }

                            // Visibility will be set after fade is finished!

                        } else {
                            // do nothing / apply original material and keep invisible
                            this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeNone;
                            //@ts-ignore
                            if (obj.material) {
                                //@ts-ignore
                                obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeNone];
                            }
                            obj.visible = false;
                        }
                    }
                    break;


                case false:
                    // hide objects specified
                    if (filteredListOfObjects.includes(obj.name)) {
                        // if visible
                        if (obj.visible) {
                            // fadeOut
                            this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeOut;
                            //@ts-ignore
                            if (obj.material) {
                                //@ts-ignore
                                obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeOut];
                            }
                        } else {
                            // do nothing / apply original material and keep invisible
                            this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeNone;
                            //@ts-ignore
                            if (obj.material) {
                                //@ts-ignore
                                obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeNone];
                            }
                            obj.visible = false;
                        }
                        // else do nothing
                        obj.visible = false;
                    } else {
                        if (obj.visible) {
                            // do nothing / apply original material and keep visibility
                            this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeNone;
                            //@ts-ignore
                            if (obj.material) {
                                //@ts-ignore
                                obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeNone];
                            }
                            obj.visible = true;
                        } else {
                            this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeIn;
                            //@ts-ignore
                            if (obj.material) {
                                // console.info("this.fadeMaterials[obj.material.name][EObjectFadeType.fadeIn]")
                                //@ts-ignore
                                // console.info(this.fadeMaterials[obj.material.name][EObjectFadeType.fadeIn])
                                //@ts-ignore
                                obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeIn];
                            }
                            // Set it visible now, so we can see the fadeIn
                            obj.visible = true;
                        }
                    }
                    break;
                default: null;
            }
        })

        // Set fadeCustom
        fadeListOfObjects.forEach(fadeObjName => {
            // find object,
            const found = this.scene.getObjectByName(fadeObjName);
            if (!found) {
                return
            }

            this.objectNameToFadeMapping[fadeObjName] = EObjectFadeType.fadeCustom;

            //@ts-ignore
            this.fadeMaterials[found.material.name].fadeCustomAmount = sData.fadeAmount

            //@ts-ignore
            found.material = this.fadeMaterials[found.material.name][EObjectFadeType.fadeCustom];

            found.visible = true;
        })
    };


    easing(type = "easeInOutSine", x: number): number {
        let c4;
        switch (type) {
            case ("easeOutCirc"):
                return Math.sqrt(1 - Math.pow(x - 1, 2));
                break;
            case ("easeOutElastic"):
                c4 = (2 * Math.PI) / 3;
                return x === 0
                    ? 0
                    : x === 1
                        ? 1
                        : Math.pow(2, -10 * x) * Math.sin((x * 10 - 0.75) * c4) + 1;
                break;
            default:
                //easeInOutSine
                return -(Math.cos(Math.PI * x) - 1) / 2;
        }
    }

    exposure(targetExposure: number, duration: number) {

        if (!this.newShot) {
            return
        }

        this.ezTweenStore[ETween.exposure] = {
            valueStart: this.scene.exposure,
            valueEnd: targetExposure,
            // valueDelta: targetExposure - this.scene.exposure,
            duration: duration,
            progress: 0,
            ease: 'easeInOutSine'
        };

    }

    envHdr(p: string = "d/cubemaps/default.hdr") {
        this.element.environmentImage = p;
    }

    lerp(a: number, b: number, alpha: number) {
        return a + alpha * (b - a)
    }

    materialTweaks(materialTweaksArray: Array<object>) {

        materialTweaksArray.forEach((matTweaksObj) => {
            //@ts-ignore
            let material = [...this.materials].find(o => o.name === matTweaksObj.materialName);

            if (!material) return;
            //@ts-ignore
            if (matTweaksObj.toneMapped != null) {
                //@ts-ignore
                material.toneMapped = matTweaksObj.toneMapped;
            };

            //@ts-ignore
            if (matTweaksObj.flatShading != null) {
                //@ts-ignore
                material.flatShading = matTweaksObj.flatShading;
            };

            //@ts-ignore
            if (matTweaksObj.emissiveIntensity != null) {
                //@ts-ignore
                material.emissiveIntensity = matTweaksObj.emissiveIntensity;
            };

            //@ts-ignore
            if (matTweaksObj.depthTest != null) {
                //@ts-ignore
                material.depthTest = matTweaksObj.depthTest;
            };

            //@ts-ignore
            if (matTweaksObj.transparent != null) {
                //@ts-ignore
                material.transparent = matTweaksObj.transparent;
            };

            //@ts-ignore
            if (matTweaksObj.opacity != null) {
                //@ts-ignore
                material.opacity = matTweaksObj.opacity;
            };

            //@ts-ignore
            if (matTweaksObj.depthWrite != null) {
                //@ts-ignore
                material.depthWrite = matTweaksObj.depthWrite;
            };

            //@ts-ignore
            if (matTweaksObj.side) {
                //@ts-ignore
                switch (matTweaksObj.side) {
                    case "front":
                        //@ts-ignore    
                        material.side = FrontSide;
                        break;
                    case "back":
                        //@ts-ignore    
                        material.side = BackSide;
                        break;
                    case "double":
                        //@ts-ignore    
                        material.side = DoubleSide;
                        break;
                    default:
                        null;

                }

            };

            //@ts-ignore
            if (matTweaksObj.blending) {
                //@ts-ignore
                switch (matTweaksObj.blending) {

                    case "noBlending":
                        //@ts-ignore    
                        material.blending = NoBlending;
                        break;
                    case "normalBlending":
                        //@ts-ignore    
                        material.blending = NormalBlending;
                        break;
                    case "additiveBlending":
                        //@ts-ignore    
                        material.blending = AdditiveBlending;
                        break;
                    case "subtractiveBlending":
                        //@ts-ignore    
                        material.blending = SubtractiveBlending;
                        break;
                    case "multiplyBlending":
                        //@ts-ignore    
                        material.blending = MultiplyBlending;
                        break;
                    default:
                        null;

                }

            };
        })
    }

    uvOffset(uvOffsetsArray: Array<object>) {

        uvOffsetsArray.forEach(uvOffsetObject => {

            //@ts-ignore
            let material: Material = [...this.materials].find(o => o.name === uvOffsetObject.materialName);

            if (!material) {
                return;
            }


            // Find sourceNode in scene
            this.scene.traverse((n) => {

                //@ts-ignore
                if (n.name === uvOffsetObject.sourceNode) {

                    // connect X,Y -> U,V
                    //@ts-ignore
                    material.map.offset =
                        n.position;


                    // Add material to updateMaterials if
                    // update is needed in update loop
                    //@ts-ignore
                    if (uvOffsetObject.animateOpacity) {
                        //@ts-ignore
                        this.uvOffsetMaterials[material.name] = {
                            material: material,
                            sourceVec: n.position
                        };
                    }


                }
            });

        })


    }

    async handleHotspots(sData) {
        if (!this.ucxMeshMapping) {
            console.info("No ucxMeshMapping");
            return;
        }

        this.removeAllChildNodes();

        //  <div slot="hotspot-hoof" class="anchor" data-surface="0 0 752 733 735 0.132 0.379 0.489"></div>

        // construct data-surface
        if (!sData?.hotspots) {
            return
        }

        Object.keys(sData.hotspots).forEach(hName => {
            //ucxMeshMapping[hName];
            if (!this.ucxMeshMapping[this.currentModel] && !this.ucxMeshMapping[this.currentModel][hName]) {
                return
            };

            const dataSurface = this.ucxMeshMapping[this.currentModel][hName] + " 0 0 1 2 1 1 1"
            console.info("Hotspots ", hName, dataSurface);

            const hDiv = document.createElement('div');
            hDiv.classList.add('anchor', 'h_container');
            hDiv.slot = `hotspot-${hName}`;
            //data-visibility-attribute="visible"
            hDiv.setAttribute("data-visibility-attribute", "visible");
            hDiv.dataset.surface = `${dataSurface}`
            hDiv.innerHTML = sData.hotspots[hName].content;

            if (sData?.hotspots[hName]?.classes) {
                hDiv.classList.add(...sData.hotspots[hName].classes);
            }

            const targetName = hName.replace("UCX_", "UCXTARGET_");
            if (this.ucxMeshMapping[this.currentModel][targetName]) {
                console.info("Hotspot has target ", targetName);

                // Add SVG Container if not exists:
                let svgContainer = this.element.querySelector('#hotspotLineContainer');
                if (svgContainer !== null && svgContainer.nodeName.toLowerCase() === "svg") {
                    console.info("Has svg container");
                } else {
                    console.info("Adding new svg container")
                    // create new svg container
                    // const svgEl = document.createElement('svg');
                    const svgEl = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
                    svgEl.id = "hotspotLineContainer";
                    svgEl.setAttribute("width", "100%");
                    svgEl.setAttribute("height", "100%");
                    // svgEl.setAttribute("xmlns", "http://www.w3.org/2000/svg");
                    // svgEl.classList.add("hotspotLineContainer");
                    this.element.appendChild(svgEl);
                    svgContainer = svgEl;

                }

                // Add UCXTARGET as hotspot div
                const dataSurface = this.ucxMeshMapping[this.currentModel][targetName] + " 0 0 1 2 1 1 1"
                const hTargetDiv = document.createElement('div');
                // hTargetDiv.classList.add('anchor', 'h_container');
                hTargetDiv.slot = `hotspot-${targetName}`
                hTargetDiv.dataset.surface = `${dataSurface}`
                // hDiv.innerHTML = sData.hotspots[hName].content;
                this.currentModel.appendChild(hTargetDiv);

                // Add line Element to SVG Container
                //<line class="line"></line>
                // const lineEl = document.createElement('line');
                const lineEl = document.createElementNS(
                    'http://www.w3.org/2000/svg',
                    'line'
                );
                lineEl.dataset.start = hName; // name of start
                lineEl.dataset.end = targetName;
                lineEl.classList.add("hotspotLine");
                svgContainer?.appendChild(lineEl);
                this.lineRenderElements.push(lineEl);

            } else { console.info("Has no target") }


            if (sData?.hotspots[hName]?.expanded) {
                hDiv.classList.add("expanded");
            }

            this.element.appendChild(hDiv);
            this.startSVGRenderLoop();
        })
    }

    update(_time: number, delta: number) {

        delta;
        // TODO We should only update this, if objects with material are visible
        if (Object.keys(this.uvOffsetMaterials).length) {
            Object.keys(this.uvOffsetMaterials).forEach(m => {
                //@ts-ignore
                this.uvOffsetMaterials[m].material.opacity = this.uvOffsetMaterials[m].sourceVec.z
            })
        }

        // Update fadeCounter and fadeMaterial opacities

        if (this.fadeInProgress) {
            this.fadeCounter = this.fadeCounter + (1 / this.fadeDuration);
            if (this.fadeCounter >= 1) {
                this.fadeInProgress = false;
                this.fadeCounter = 0;
                this.resetFadeMaterials();
                return;
            }
            Object.keys(this.fadeMaterials).forEach((m) => {
                if (0 + this.fadeCounter >= 1 || 1 - this.fadeCounter <= 0) {
                    return
                }
                //@ts-ignore
                this.fadeMaterials[m].fadeIn.opacity = 0 + this.fadeCounter;
                //@ts-ignore
                this.fadeMaterials[m].fadeOut.opacity = 1 - this.fadeCounter;

                //@ts-ignore
                if (0 + this.fadeCounter < this.fadeMaterials[m].fadeCustomAmount) {
                    this.fadeMaterials[m].fadeCustom.opacity = 0 + this.fadeCounter;
                }
            })
        }


        // Update Tweens
        Object.keys(this.ezTweenStore).forEach(s => {

            const tweenType: ETween = s as ETween;

            const tween = this.ezTweenStore[tweenType];

            if (!tween) { return };

            // Remove finished Tweens
            if (tween.progress >= 1) {

                delete this.ezTweenStore[tweenType];
            }

            tween.progress = tween.progress + (1 / tween.duration);


            switch (tweenType) {
                case 'exposure':
                    this.scene.exposure = this.lerp(tween.valueStart, tween.valueEnd, this.easing("easeInOutSine", tween.progress));
                    break
                default:
                    null
            }
        })
    }
}