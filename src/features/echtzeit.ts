// import {property} from 'lit/decorators.js';
import { AxesHelper, BackSide, BoxGeometry, DoubleSide, FrontSide, Material, Mesh, MeshBasicMaterial, Object3D, Quaternion, SphereGeometry, TextureLoader, Vector3 } from 'three';
import ModelViewerElementBase, { $scene, $tick } from '../model-viewer-base.js';
import { Constructor } from '../utilities.js';
import { EchtzeitAnim } from './echtzeitAnim.js';
// import { ModelViewerElement } from '../model-viewer.js';

export declare interface EzInterface {
    shot(shotData: object, assetsUrl: string | null, backgroundContainer: HTMLDivElement | null, dev: boolean): void,
    ezShot(id: string): void,
    setVisualizer(state: boolean): void
}


interface IComponentLibrary {
    [key: string]: Object3D
}


interface IObjectToMaterialMapping {
    [key: string]: string;
}

interface ITweenItem {
    valueStart: number,
    valueEnd: number,
    // valueDelta: number,
    duration: number,
    progress: number,
    ease: string
}

enum ETween {
    exposure = "exposure"
}

type ITweenStore = {
    [key in ETween]?: ITweenItem
}

interface IuvOffsetMaterialItem {
    material: Material,
    sourceVec: Vector3,

}
interface IuvOffsetMaterials {
    [key: string]: IuvOffsetMaterialItem
}

enum EObjectFadeType {
    fadeIn = "fadeIn",
    fadeOut = "fadeOut",
    fadeCustom = "fadeCustom",
    fadeNone = "fadeNone" // use the original material
}

interface IFadeMaterials {
    [key: string]: {
        //[key in EObjectFadeType]: Material
        fadeIn: Material,
        fadeOut: Material,
        fadeCustom: Material,
        fadeNone: Material,
        fadeCustomAmount: number
    }
}
interface IObjectNameToFadeMapping {
    [key: string]: EObjectFadeType
}

export const rctRoot = [new Object3D()];




export const EchtzeitMixin = <T extends Constructor<ModelViewerElementBase>>(ModelViewerElement: T): Constructor<EzInterface> & T => {

    class EchtzeitModelViewerElement extends ModelViewerElement {
        initEvent: Event;
        readyInterval;
        currentShot: string | null = null;
        newShot: boolean = false;
        assetsPath: string | null = null;
        utilVec = new Vector3();
        componentLib: IComponentLibrary = {};
        rctNodes: Array<Object3D> = []; // raycastTarget nodes. Nodes that can be clicked.
        ucxNodes: Array<Object3D> = [];
        hookNodes: Array<Object3D> = [];
        hookVisNodes: Array<Object3D> = [];
        fadeMaterials: IFadeMaterials = {};
        objectToMaterialMapping: IObjectToMaterialMapping = {};
        fadeCounter: number = 0;
        fadeDuration: number = 40;
        fadeInProgress: boolean = false;
        objectNameToFadeMapping: IObjectNameToFadeMapping = {};
        isFading: Array<string> = [];
        ezTweenStore: ITweenStore = {};
        materials = new Set();
        enableObjectNames: Set<unknown> = new Set();
        enableNodes: Array<Object3D> = []; // THREE.Object3D Scene nodes that can be enabled/disabled from a shot
        uvOffsetMaterials: IuvOffsetMaterials = {};
        dev = false;
        shotData: object | null = null;
        shotID: string | null = null;
        ucxMeshMapping: object = {};
        gltfLoadData: object = {};
        collections: object = {};
        isLoading = false;
        // progressScreen: HTMLDivElement | null = null;
        // progressBarWrapper: HTMLDivElement | null = null;
        // progressBar: HTMLDivElement | null = null;
        // progressAmount: HTMLSpanElement | null = null;
        loaderWrapper: HTMLDivElement | null = null;
        loaderAmountSpan: HTMLSpanElement | null = null;
        lineRenderElements: Array<HTMLOrSVGElement> = [];
        currentModel: string | null = null;
        currentOrbit: object | null = null;
        nextOrbit: object | null = null;
        currentCameraTarget: object | null = null;
        nextCameraTarget: object | null = null;
        animGraphCurrent: object | null = null;
        animGraphToPlay: object | null = null;
        backgroundContainer: HTMLDivElement | null = null;
        sceneTargetVisualizer: AxesHelper | null = null;
        webPSupported: boolean = true;
        animHandler: EchtzeitAnim;

        constructor(...args: Array<any>) {
            super(...args);


            this.supportsWebp();

            //@ts-ignore
            this.exposure = 0;

            this.addEventListener('camera-change', this.handleCameraInteraction.bind(this));
            this.addEventListener('progress', this.updateProgressbar);
            // this.addEventListener('finished', this.handleAnimationFinished);

            // Ready Event: Send event once, then start timer 
            // and send until shot received:
            this.initEvent = new Event('echtzeitReady');

            this.dispatchEvent(this.initEvent);

            this.readyInterval = setInterval(this.sendReadyEvent, 1000);

            // Accessing ModelScenes animation mixer
            // Weight blending see here: 
            // https://github.com/mrdoob/three.js/blob/master/examples/webgl_animation_skinning_blending.html
            // console.info(this[$scene]['mixer']);

            // collect existing spotlights
            // has light changed?
            // compare lights? id for lights?
            // if no light, fadeout and dispose

            // const splight = new SpotLight( 0xFF7F00, 200 );
            // splight.position.set( 1, 1, 1 );
            // splight.castShadow = true;
            // splight.angle = 1;
            // splight.penumbra = 0.2;
            // splight.decay = 2;
            // splight.distance = 50;
            // const splightHlp = new SpotLightHelper(splight);
            // this[$scene].target.add(splight,splightHlp);

            // update helper

            this.animHandler = new EchtzeitAnim(this[$scene], this.lerp, this.easing);

        }

        async supportsWebp() {
            if (!self.createImageBitmap) return false;
            const webpData = 'data:image/webp;base64,UklGRioAAABXRUJQVlA4IB4AAABwAQCdASoBAAEAAoBCJZQCdAGIQAD+6tRWkQ6VBAA=';
            const blob = await fetch(webpData).then(r => r.blob());
            return createImageBitmap(blob).then(() => this.webPSupported = true, () => this.webPSupported = false);
        }

        sendReadyEvent = () => {
            if (this.currentShot) {
                if (this.dev) { console.info("Shot received, clearing readyInterval") }
                clearInterval(this.readyInterval);
                return;
            }
            this.dispatchEvent(this.initEvent);
        }

        async buildShot(id: string) {
            let shotEl = document.querySelector('ez-shot#' + id);
            if (!shotEl) return;

            const shotId = shotEl.id;

            // the closest parent with data-defs
            const shotDefsAncestor = shotEl.closest('ez-shot[data-defs]');
            if (!shotDefsAncestor) return;
            //@ts-ignore
            const shotDefs = shotDefsAncestor.dataset.defs;

            // Chain all data-classes by going up the DOM 
            // until parent with ez-shot>data-defs
            const classes = []
            while (shotEl != shotDefsAncestor) {
                // split by whitespace or comma
                //@ts-ignore
                classes.unshift(...shotEl.dataset.class.split(/[ ,]+/))
                //@ts-ignore
                shotEl = shotEl.parentNode;
            }
            //@ts-ignore
            classes.unshift(...shotEl.dataset.class.split(/[ ,]+/))

            const defsData = await import('/' + shotDefs + '.js');
            const defs = defsData.default;

            if (!defs) return;

            const newShot = { id: shotId };

            classes.forEach(c => {
                if (c in defs) {
                    defs[c](newShot);
                }
            })

            return newShot;

        }

        /**
         * Trigger a shot build and run the shot
         * @param id 
         */
        async ezShot(id: string) {
            const shotData = await this.buildShot(id);
            if (shotData) {
                this.shot(shotData);
            }
        }

        createProgressBar() {
            // Add new loader if not exist
            if (this.loaderWrapper) {
                if (this.dev) { console.info("loader screen exists") };
                return
            };


            // Create Style
            let style: HTMLElement | null = document.head.querySelector('#progress-style');

            if (!style) {
                // if not found, create it
                const css = `

                    .loader-wrapper {
                        position: fixed;
                        top:5vh;
                        left:50%;
                        transform: translateX(-50%);
                        padding: 0.25rem;
                        padding-top: 0.2rem;
                        display: flex;
                        align-items: flex-end;
                        flex-flow: row nowrap;
                        background-color: #0a0a0a;
                        opacity:.75;
                        border-radius: 0.5rem;
                    }
                    .loader-circle-stack {
                        margin-right: 0.25rem;
                        position: relative;
                        width: 1rem;
                        height: 1rem;
                    }
                    .loader-circle {
                        width: 1rem;
                        height: 1rem;
                        margin-right: 0.5rem;
                        border-radius: 50%;
                        box-shadow: inset 0 0 0 2px rgba(255, 255, 255, 0.1);
                    }
                    
                    .loader-circle-mask {
                        position: absolute;
                        top: 0;
                        left: 0;
                        width: 0.5rem;
                        height: 1rem;
                        margin-right: 0.5rem;
                        overflow: hidden;
                        transform-origin: 0.5rem 0.5rem;
                        -webkit-mask-image: -webkit-linear-gradient(
                            top,
                            rgba(0, 0, 0, 1),
                            rgba(0, 0, 0, 0)
                        );
                        animation: loader-rotate 1.2s infinite linear;
                        
                        }
                    .loader-circle-line {
                            width: 1rem;
                            height: 1rem;
                            border-radius: 50%;
                            box-shadow: inset 0 0 0 2px rgba(255, 255, 255, 0.5);
                        }
                    
                    .loader-amount {
                        text-transform: uppercase;
                        color: #d4d4d4;
                        font-weight: 400;
                        font-size: 0.875rem;
                        line-height: 1.25rem;
                        font-family: ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont,
                        "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif,
                        "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
                        animation: loader-fade 1.2s infinite ease-in-out;
                    }

                    .loader-percent{
                        color:#e5e5e5;
                    }

                    @keyframes loader-rotate {
                        0% {
                        transform: rotate(0deg);
                        }
                        100% {
                        transform: rotate(360deg);
                        }
                    }
                    
                    @keyframes loader-fade {
                        0% {
                        opacity: 1;
                        }
                        50% {
                        opacity: 0.5;
                        }
                    }

                `;

                const head = document.head || document.getElementsByTagName('head')[0];

                style = document.createElement('style');

                style.id = 'progress-style';

                head.appendChild(style);
                //@ts-ignore
                style.type = 'text/css';
                //@ts-ignore
                if (style.styleSheet) {
                    // This is required for IE8 and below.
                    //@ts-ignore
                    style.styleSheet.cssText = css;
                } else {
                    style.appendChild(document.createTextNode(css));
                }
            }



            this.loaderWrapper = document.createElement('div');
            const loaderCircleStack = document.createElement('div');
            const loaderCircle = document.createElement('div');
            const loaderCircleMask = document.createElement('div');
            const loaderCircleLine = document.createElement('div');
            const loaderAmount = document.createElement('div');
            this.loaderAmountSpan = document.createElement('span');

            this.loaderWrapper.classList.add("loader-wrapper");
            loaderCircleStack.classList.add("loader-circle-stack");
            loaderCircleMask.classList.add("loader-circle-mask");
            loaderCircle.classList.add("loader-circle");
            loaderCircleLine.classList.add("loader-circle-line");
            loaderAmount.classList.add("loader-amount");
            this.loaderAmountSpan.classList.add("loader-percent");

            loaderAmount.innerText = "Loading 3D ";

            loaderAmount.appendChild(this.loaderAmountSpan);
            loaderCircleMask.appendChild(loaderCircleLine);
            loaderCircleStack.appendChild(loaderCircle);
            loaderCircleStack.appendChild(loaderCircleMask);
            this.loaderWrapper.appendChild(loaderCircleStack);
            this.loaderWrapper.appendChild(loaderAmount);
            this.appendChild(this.loaderWrapper);
        }

        //@ts-ignore
        updateProgressbar = (event) => {
            if (this.dev) {
                console.info(event.detail.totalProgress);
            }
            if (!this.loaderAmountSpan) {
                return;
            }
            //this.progressBar.style.width = event.detail.totalProgress * 100 + "%";
            this.loaderAmountSpan.innerText = Math.trunc(event.detail.totalProgress * 100) + "%";
        }

        removeAllChildNodes() {
            // Remove all annotations and hotspots
            const annot = this.querySelectorAll('[slot^="hotspot"]');
            const svg = this.querySelectorAll('svg');
            if (annot.length) {
                //@ts-ignore
                for (const el of annot) {
                    el.parentNode.removeChild(el);
                }
            }

            if (svg.length) {
                //@ts-ignore
                for (const el of svg) {
                    el.parentNode.removeChild(el);
                }
            }

            this.lineRenderElements.length = 0;
        }

        shot(shotData: object, assetsUrl: string | null = null, backgroundContainer: HTMLDivElement | null = null, dev: boolean = false) {

            if (!shotData) return;
            this.dev = dev;
            this.backgroundContainer = backgroundContainer;
            this.shotData = shotData;

            if (assetsUrl && assetsUrl.endsWith("/")) {
                this.assetsPath = assetsUrl;
            }

            // Add a poster frame during load, if the poster-attribute is not present yet.
            // If another shot is triggered during load, the poster-attribute will be set again,
            // but the poster will not load (because the glb is loading?).
            // Workaround for now is to keep the first poster that was set: if, during load, the shot
            // changes, only set the poster-attribute, if it is not yet present. 
            // After load, the poster-attribute will be deleted. 
            //@ts-ignore
            if (!this.hasAttribute("poster") && this.shotData.poster) {
                let extension = '.webp';
                if (!this.webPSupported) extension = '.png';

                if (this.assetsPath) {
                    //@ts-ignore
                    this.setAttribute('poster', this.assetsPath + this.shotData.poster + extension);
                } else {
                    //@ts-ignore
                    this.setAttribute('poster', this.shotData.poster + extension);
                }
            }

            // Run background animation here, because we want it 
            // to be behind the poster frame
            this.handleBackground(this.shotData);

            //@ts-ignore
            this.shotID = this.shotData.id;
            //@ts-ignore
            if (this.shotData.load) {
                const clonedData = JSON.parse(JSON.stringify(this.shotData));
                this.loadModel(clonedData);
                return;
            }
            // If no model loaded and no .load data in shot, do nothing
            if (this.currentModel) {
                // No model loaded.
                if (this.dev) {
                    console.info("No model loaded and no model to load.")
                }
                return;
            }
            this.applyShot();
        }

        async loadModel(s: object) {

            const loadFinishedHandler = () => {
                if (this.dev) {
                    console.log('New model loaded');
                }

                

                //@ts-ignore
                if (s.lightmap) {
                    //@ts-ignore
                    const lightmapArray = s.lightmap;
                    //@ts-ignore
                    lightmapArray.forEach(lightmapObject => {
                        // lightmapObject.material
                        // lightmapObject.lightmap


                        this[$scene].traverse(function (object) {
                            //@ts-ignore
                            if (object.material?.name === lightmapObject.material) {
                                const lightmap = new TextureLoader().load(lightmapObject.lightmap);
                                lightmap.flipY = false;
                                //@ts-ignore
                                lightmap.channel = 1;
                                //@ts-ignore
                                object.material.lightMap = lightmap;
                                //@ts-ignore
                                object.material.lightMapIntensity = lightmapObject.intensity;
                            };

                        });

                        //@ts-ignore
                        // let material = [...this.materials].find(o => o.name === lightmapObject.material);



                    })


                    //@ts-ignore
                    // room.children[1].children[0].material.lightMap = lightmap;
                }

                this.currentModel = loadInfo.model;

                this.isLoading = false;

                if (this.loaderWrapper) {
                    this.loaderWrapper.remove();
                    this.loaderWrapper = null;
                }

                //@ts-ignore
                this.currentOrbit = this.getCameraOrbit();
                //@ts-ignore
                this.currentCameraTarget = this.getCameraTarget();

                this.clearAndCollect();

                // mv starts with animations paused. This unpauses by
                // calling  animation.ts this[$paused] = false;
                //@ts-ignore
                this.play();

                // Initialise animation
                this.animHandler.initialiseAnimHandler();

                // this[$scene].updateAnimation(0);
                // this[$needsRender]();

                if (this.dev) {
                    this.sceneHierarchy();

                }
                // Apply shot with inital anim
                this.applyShot(true);

                // Clear poster frame
                this.removeAttribute("poster");

            };


            // Shot.load is array for additive loading. For now, we can only load one model. Therefore
            // we only load the first one.
            //@ts-ignore
            if (!s?.load?.length) {
                if (this.dev) {
                    console.info("No model to load");
                }
                this.applyShot();
                return
            }

            // s.load[0] is the path to the glb e.g. static/something/mygltf_merged.glb
            //@ts-ignore
            var gltfName = s.load[0].substring(s.load[0].lastIndexOf('/') + 1);
            //const gltfName = s.load[0] 

            if (this.currentModel === gltfName) {
                if (this.dev) { console.log('Model already loaded') };
                this.applyShot();
                return;
            }

            // Get the header file containing loadData, collections, ucx meshmapping
            //@ts-ignore
            let gltfHeadersFile = s.load[0].replace('.glb', '_headers.json');
            if (this.assetsPath) {
                gltfHeadersFile = this.assetsPath + gltfHeadersFile;
            }
            const res = await fetch(gltfHeadersFile);
            const data = await res.json();



            if (res.ok) {
                //@ts-ignore
                this.ucxMeshMapping[gltfName] = data.mv_ucx_mesh_mapping[gltfName];
                //@ts-ignore
                this.gltfLoadData[gltfName] = data.gltf_load[gltfName];
                //@ts-ignore
                this.collections[gltfName] = data.collections[gltfName];
                if (this.dev) {
                    console.info("Success fetching gltf header!");
                }
            } else {
                if (this.dev) {
                    console.info("Could not fetch gltf header data")
                }
                throw new Error(data);
            }

            if (!this.gltfLoadData) {
                if (this.dev) {
                    console.info("No gltfLoadData");
                }
                return
            }

            //@ts-ignore
            const loadInfo = this.gltfLoadData[gltfName];


            if (!loadInfo) {
                if (this.dev) {
                    console.info("Model to load is not in list")
                }
                this.applyShot();
                return;
            }


            this.isLoading = true;

            //@ts-ignore
            this.showPoster();

            this.removeAllChildNodes();

            this.createProgressBar();

            // set the new source
            if (this.assetsPath) {
                // @ts-ignore
                this.src = this.assetsPath + s.load[0];
            } else {
                // @ts-ignore
                this.src = s.load[0];
            }
            //@ts-ignore
            this.minCameraOrbit = `auto auto 0.1m`;
            //@ts-ignore
            this.maxCameraOrbit = `auto auto auto`;
            //@ts-ignore
            this.maxFieldOfView = '150deg'; //data.fov;
            //@ts-ignore
            this.minFieldOfView = '10deg';

            this.removeEventListener('load', loadFinishedHandler.bind(this));
            this.addEventListener('load', loadFinishedHandler.bind(this));

        }

        isShotNew(shotID: string) {
            if (shotID != this.currentShot) {
                this.newShot = true;
                this.currentShot = shotID;
            } else {
                this.newShot = false;
            }
        }

        sceneHierarchy() {
            const scn = this[$scene]
            this[$scene].traverse(function (obj: Object3D) {

                var s = '|___';

                var obj2: Object3D = obj;

                while (obj2 !== scn) {

                    s = '\t' + s;
                    //@ts-ignore
                    obj2 = obj2.parent;

                }
                //@ts-ignore
                if (obj.material) {
                    //@ts-ignore
                    console.info(s + obj.name + ' <' + obj.type + '>' + '  Mat: ' + obj.material.name);
                } else {
                    console.info(s + obj.name + ' <' + obj.type + '>')
                }

            });

        }

        setVisualizer(state = false): void {
            // Dont create if not exist and user doesnt want to show it
            if (!this.sceneTargetVisualizer && !state) return;


            if (!this.sceneTargetVisualizer) {
                // create
                this.sceneTargetVisualizer = new AxesHelper(5);
                this[$scene].target.add(this.sceneTargetVisualizer);
            }


            if (state) {
                this.sceneTargetVisualizer.visible = true;
            } else {
                this.sceneTargetVisualizer.visible = false;
            }

            //axesHelper.position.copy(this.scene.target.position);


        }

        buildObjectSet() {
            // Build set with all objectnames from collections data.
            // We need a Set because Objects can be in several collections. 

            const objectSet = new Set();
            if (!this.collections) {
                return;
            }
            Object.keys(this.collections).forEach((gltfName) => {
                //@ts-ignore
                Object.keys(this.collections[gltfName]).forEach((collectionName) => {
                    //@ts-ignore
                    this.collections[gltfName][collectionName].forEach((o) => {
                        objectSet.add(o);
                    });
                });
            });

            this.enableObjectNames = objectSet;
        }

        clearAndCollect() {

            // Clear
            for (const prop of Object.getOwnPropertyNames(this.componentLib)) {
                delete this.componentLib[prop];
            }

            this.rctNodes.length = 0;
            this.hookNodes.length = 0;
            this.hookVisNodes.length = 0;

            this.enableObjectNames.clear();
            this.enableNodes.length = 0;
            this.materials.clear();

            // Clear Fading stuff
            for (var member in this.objectNameToFadeMapping) delete this.objectNameToFadeMapping[member];

            this.isFading.length = 0;
            for (var member in this.fadeMaterials) delete this.fadeMaterials[member];
            for (var member in this.objectToMaterialMapping) delete this.objectToMaterialMapping[member];

            this.buildObjectSet();

            // Collect enableNodes and UCX nodes:
            this[$scene].traverse((n) => {

                if (n.name.startsWith('UCX_') || n.name.startsWith('UCXTARGET_') || n.name.startsWith('UCXLINE_')) {

                    this.ucxNodes.push(n);

                } else {
                    // Check if the node is in one of the collections
                    if (this.enableObjectNames.has(n.name)) {
                        this.enableNodes.push(n);
                        this.objectNameToFadeMapping[n.name] = EObjectFadeType.fadeNone;

                        //@ts-ignore
                        if (n.material) {

                            // @ts-ignore
                            this.materials.add(n.material)


                            // Also collect into fadeMaterials.
                            //@ts-ignore
                            this.fadeMaterials[n.material.name] = {
                                //@ts-ignore
                                fadeNone: n.material, //if object fade is set to fadeNone, apply this.
                                //@ts-ignore
                                fadeIn: n.material.clone(),
                                //@ts-ignore
                                fadeOut: n.material.clone(),
                                //@ts-ignore
                                fadeCustom: n.material.clone(),
                                fadeCustomAmount: 0.5
                            }
                            //@ts-ignore
                            this.fadeMaterials[n.material.name].fadeIn.transparent = true;
                            //@ts-ignore
                            this.fadeMaterials[n.material.name].fadeOut.transparent = true;
                            //@ts-ignore
                            this.fadeMaterials[n.material.name].fadeCustom.transparent = true;
                        };


                    }
                }
            });

            this.ucxNodes.forEach(n => n.visible = false)


            // collect RCT_ (raycastTarget)
            this[$scene].traverse((n) => {

                if (n.name.startsWith('RCT_')) {
                    this.rctNodes.push(n);

                }
            });
            //this.scene.rctNodes.forEach(n => n.visible = false)


            // collect HOOK_
            this[$scene].traverse((n) => {

                if (n.name.startsWith('HOOK_')) {
                    this.hookNodes.push(n);

                }
            });
            //this.scene.hookNodes.forEach(n => n.visible = false)



            // collect componentLibrary

            this[$scene].traverse((n) => {

                if (n.name.startsWith('CMP_')) {
                    // Register as component
                    // hide

                    this.componentLib[n.name] = n; // defined in ModelScene

                    //console.info("Registered ", n.name, " in component library")
                    n.visible = false
                }

            });
        }

        visualizeNodes() {
            let dir = new Vector3();
            let worldQuaternion = new Quaternion(); // create once and reuse it

            this.rctNodes.forEach(r => {
                const geometry = new BoxGeometry(1, 1, 1);
                const material = new MeshBasicMaterial({ color: 0xfacc15 });
                const rctGeo = new Mesh(geometry, material);
                r.add(rctGeo);
            })

            this.hookNodes.forEach(h => {

                h.getWorldQuaternion(worldQuaternion);
                dir.copy(h.up).applyQuaternion(worldQuaternion);


                const geometry = new SphereGeometry(1, 8, 8);
                const material = new MeshBasicMaterial({ color: 0xb91c1c });
                const hookGeo = new Mesh(geometry, material);
                h.add(hookGeo);

                this.hookVisNodes.push(hookGeo);

            })

        }

        //@ts-ignore
        handleCameraInteraction(event) {
            if (event.detail.source === 'user-interaction') {
                // Camera changed by user
                //@ts-ignore
                this.interpolationDecay = 50;
            } else {
                //@ts-ignore
                this.interpolationDecay = 200;
            }
        }

        //@ts-ignore
        handleAnimationFinished = (event) => {
            // console.info(this.modelviewer.animationName)
            // console.info("Loop count ", event.detail.count);

            // split animationname by @ to get rigname 
            // this.animGraphToPlay
            //@ts-ignore
            if (!this.animationName) {
                return;
            }
            //@ts-ignore
            const rigName = this.animationName.split('@')[0];
            //@ts-ignore
            this.handleAnimation(this.animGraphToPlay[rigName]);

        }

        applyShot(initialAnim = false) {
            const clonedData = JSON.parse(JSON.stringify(this.shotData));
            const sData = clonedData;

            if (!this.currentModel) return;
            if (!this.shotID) return;
            this.isShotNew(this.shotID);

            if (sData.hdr) {
                this.envHdr(sData.hdr);
            }

            if (initialAnim) {
                // apply camera settings
                if(sData?.maxCamOrbit){
                    //@ts-ignore
                    this.maxCameraOrbit = sData.maxCamOrbit.join(' ');
                }

                if(sData?.maxFOV){
                    //@ts-ignore
                    this.maxFieldOfView = sData.maxFOV;
                }

                // When model is loaded, apply materialTweaks and uvOffsets:
                if (sData?.materialTweaks) {
                    // console.info("MaterialTweaks")
                    this.materialTweaks(sData.materialTweaks);
                }

                if (sData?.uvOffset) {
                    this.uvOffset(sData.uvOffset);
                }
            }



            // set camera and env
            if (sData?.animation) {
                Object.keys(sData.animation).forEach(entityName => {
                    const anim = sData?.animation[entityName];


                    switch (anim.type) {
                        case 'camera':
                            //@ts-ignore
                            this.currentOrbit = this.getCameraOrbit();

                            //const clonedData = JSON.parse(JSON.stringify(this.currentOrbit));
                            //@ts-ignore
                            this.nextOrbit = Object.assign({}, this.currentOrbit); // Shallow clone!
                            //@ts-ignore
                            this.currentCameraTarget = this.getCameraTarget();

                            this.nextCameraTarget = Object.assign({}, this.currentCameraTarget);
                            // this.nextCameraTarget = cloneDeep(this.currentCameraTarget);
                            // azimuthal = theta
                            // polar = phi (phi is measured down from the top)
                            if (anim.keys[0]?.camera?.float?.yaw != null) {
                                //@ts-ignore
                                this.nextOrbit.theta = this.degreesToRadians(anim.keys[0].camera.float.yaw);
                            }
                            if (anim.keys[0]?.camera?.float?.pitch != null) {
                                //@ts-ignore
                                this.nextOrbit.phi = this.degreesToRadians(anim.keys[0].camera.float.pitch);
                            }
                            if (anim.keys[0]?.camera?.float?.distance != null) {
                                //@ts-ignore
                                this.nextOrbit.radius = anim.keys[0].camera.float.distance;
                            }

                            if (anim.keys[0]?.camera?.float?.near != null) {
                                this[$scene].camera.near = anim.keys[0].camera.float.near;
                            }

                            if (anim.keys[0]?.camera?.float?.far != null) {
                                this[$scene].camera.far = anim.keys[0].camera.float.far;
                            }



                            if (anim.keys[0]?.camera?.pivotPoint != null) {


                                if (typeof anim.keys[0].camera.pivotPoint === "string") {
                                    // find object in scene and get its position
                                    const s = anim.keys[0].camera.pivotPoint;
                                    // console.info("Is string ", s)
                                    const foundPosition = this.worldPosFromObjectName(s)
                                    if (foundPosition) {
                                        anim.keys[0].camera.pivotPoint = foundPosition;
                                    }

                                }
                                if (anim.keys[0]?.camera?.pivotPoint?.x != null) {
                                    //@ts-ignore
                                    this.nextCameraTarget.x = anim.keys[0].camera.pivotPoint.x;
                                    //@ts-ignore
                                    this.nextCameraTarget.y = anim.keys[0].camera.pivotPoint.y;
                                    //@ts-ignore
                                    this.nextCameraTarget.z = anim.keys[0].camera.pivotPoint.z;
                                }
                            }

                            if (anim.keys[0]?.camera?.fov != null) {
                                //if (anim.keys[0]?.camera?.minFov != null) {
                                //    this.modelviewer.minFieldOfView=anim.keys[0].camera.minFov.toString() + "deg";
                                //}
                                //if (anim.keys[0]?.camera?.maxFov != null) {
                                //    this.modelviewer.maxFieldOfView=anim.keys[0].camera.maxFov.toString() + "deg";
                                //}
                                //@ts-ignore
                                this.fieldOfView = anim.keys[0].camera.fov.toString() + "deg";

                            }

                            // this.modelviewer.interpolationDecay = 50;

                            //@ts-ignore
                            this.cameraOrbit = this.nextOrbit.toString();
                            //@ts-ignore
                            this.cameraTarget = this.nextCameraTarget.toString();
                            break
                        case 'env':
                            if (this.dev) {
                                //@ts-ignore
                                console.info("Current exposure: ", this.exposure)
                            }
                            if (anim.keys[0]?.env?.skyboxIntensity != null) {
                                this.handleExposure(anim.keys[0]?.env?.skyboxIntensity, anim.keys[0]?.duration ? anim.keys[0].duration : 80)
                            }
                            break;

                        default:
                            if (this.dev) {
                                console.info("No Animation Handler for type ", anim.type)
                            }
                    }
                })

            }

            // Handle visibilities. Will set enableNodes and fading objects
            this.handleVisibilities(sData, this.currentModel)

            if (sData?.wAnims) {
                this.animHandler.handleNewWeights(sData.wAnims);
            }

            // Check if animGraph in shot is already playing.
            // Compare
            /*
            let sameAnimGraph = false;
            if (
                (JSON.stringify(this.animGraphCurrent) === JSON.stringify(sData.animGraph))) {
                // console.info("Shot has same animGraph.")
                sameAnimGraph = true;
            }
            */

            /*
             if (initialAnim) {
 
                 // When model is loaded, run its first animation.
 
                 // For each gltf in .load, add its first anim at start of animGraph Array.
                 // There can be an existing animGraph, or not.
                 if (sData?.load?.length) {
                     //@ts-ignore
                     sData.load.forEach(gltfPath => {
                         const gltf = gltfPath.substring(gltfPath.lastIndexOf('/') + 1);
                         //@ts-ignore
                         if (!Array.isArray(this.gltfLoadData[gltf].animation)) {
                             return;
                         }
                         //@ts-ignore
                         const rigName = this.gltfLoadData[gltf].rigname;
                         //@ts-ignore
                         const initAnimName = this.gltfLoadData[gltf].animation[0].name;
 
                         // Does shot have animGraph.rigname?
                         if (Array.isArray(sData.animGraph[rigName])) {
                             // True -> add init anim at start
                             sData.animGraph[rigName].unshift({ name: initAnimName });
                         } else {
                             // False -> add rigname:[{anim}]
                             if (!sData.animGraph) {
                                 sData.animGraph = {}
                             }
                             sData.animGraph[rigName] = [{ name: initAnimName }]
                         }
                     })
                 }
             }
             
             */
            /*
            if (sData?.animGraph && !sameAnimGraph) {

                this.animGraphCurrent = JSON.parse(JSON.stringify(sData.animGraph));
                this.animGraphToPlay = JSON.parse(JSON.stringify(sData.animGraph));
                // Loop over rignames:
                //@ts-ignore
                Object.keys(this.animGraphToPlay).forEach(rigName => {
                    //@ts-ignore
                    const graph = this.animGraphToPlay[rigName];
                    this.handleAnimation(graph);
                })
            }
            */

            if (!sData?.keepHotspots) {
                this.handleHotspots(sData);
            }

            if (initialAnim) {
                // Remove poster frame
                if (this.dev) {
                    console.info("Dismiss poster")
                }
                //@ts-ignore
                this.dismissPoster();
            }


            //@ts-ignore
            this[$scene].isDirty = true;
        }

        handleBackground(sData: any) {
            if (sData.background) {
                if (!this.backgroundContainer) {
                    //@ts-ignore
                    this.backgroundContainer = document.getElementById("ezGradientContainer");
                }


                if (!this.backgroundContainer) {
                    return
                }

                // Create Style
                let style: HTMLElement | null = document.head.querySelector('#ez-bg-style');

                if (!style) {
                    // if not found, create it
                    const css = `
                    .ezBG {
                        position: absolute;
                        width: 100%;
                        height: 100%;
                        top: 0;
                        left: 0;
                      }
                        @keyframes ez-bg-fadeIn {
                            from {
                            opacity: 0;
                            }
                        
                            to {
                            opacity: 1;
                            }
                        }
                `;

                    const head = document.head || document.getElementsByTagName('head')[0];

                    style = document.createElement('style');

                    style.id = 'ez-bg-style';

                    head.appendChild(style);
                    //@ts-ignore
                    style.type = 'text/css';
                    //@ts-ignore
                    if (style.styleSheet) {
                        // This is required for IE8 and below.
                        //@ts-ignore
                        style.styleSheet.cssText = css;
                    } else {
                        style.appendChild(document.createTextNode(css));
                    }
                }

                // Add new element, fade in
                const x = sData.background.X
                const y = sData.background.Y
                const cols = sData.background.colors.join(",")

                const newBG = document.createElement("div");

                // Class set in app.scss
                newBG.classList.add("ezBG");

                newBG.style.backgroundImage = `radial-gradient(circle at ${x}% ${y}%, ${cols}),url("./grain.jpg")`;
                newBG.style.width = "100%";
                newBG.style.height = "100%";
                newBG.style.animationDuration = "2s";
                newBG.style.animationName = "ez-bg-fadeIn";
                newBG.style.animationTimingFunction = "ease-out";
                this.backgroundContainer.appendChild(newBG);

                // Remove previousSibling when anim done
                //@ts-ignore
                newBG.addEventListener("animationend", () => { if (event.target.previousSibling) { event.target.previousSibling.remove() } });

                // add fadeIn class
                // newBG.classList.add("ezGradientFade");
            }
        }

        async handleAnimation(animGraph: Array<object>) {
            // handle no Array
            if (!Array.isArray(animGraph)) {
                if (this.dev) {
                    console.info("animGraph not Array")
                }
                return;
            }
            // handle no length
            if (!animGraph.length) {
                // console.info("End of animGraph")
                return;
            }


            // Play first in list and remove it from list.
            // PLAY ONCE and LOOP
            // Modelviewer might be too slow to catch the end of an animation before it loops.
            // Therefore we need to set every anim that is not the last in the graph to only play ONCE!

            const animToPlay = animGraph.shift();
            // Still entries left in animGraph?
            let loop = false;
            if (!animGraph.length) {
                loop = true;
            }
            //@ts-ignore
            this.animationName = animToPlay.name;
            //@ts-ignore
            if (animToPlay.speed != null) {
                //@ts-ignore
                this.timeScale = animToPlay.speed;
            }
            //@ts-ignore
            if (animToPlay.crossfade != null) {
                //@ts-ignore
                this.animationCrossfadeDuration = animToPlay.crossfade;
            }
            await this.updateComplete;

            if (loop) {
                //@ts-ignore
                this.play();
            } else {
                //@ts-ignore
                this.play({ repetitions: 1 });
            }
            if (this.dev) {
                //@ts-ignore
                console.info("Playing anim ", animToPlay.name, " as loop:", loop);
            }
            return;
        };

        startSVGRenderLoop = () => {
            // If no lines to draw, return;
            if (!this.lineRenderElements.length) {
                if (this.dev) {
                    console.info("SVG Renderloop stopped.")
                }
                return;
            }
            // set x1, y1 and x2,y2 on line
            this.lineRenderElements.forEach((line) => {
                // get data start and end
                //@ts-ignore
                const start = this.queryHotspot('hotspot-' + line.dataset.start);
                //@ts-ignore
                const end = this.queryHotspot('hotspot-' + line.dataset.end);
                //@ts-ignore
                line.setAttribute('x1', start?.canvasPosition?.x);
                //@ts-ignore
                line.setAttribute('y1', start?.canvasPosition?.y);
                //@ts-ignore
                line.setAttribute('x2', end?.canvasPosition?.x);
                //@ts-ignore
                line.setAttribute('y2', end?.canvasPosition?.y);

            })
            requestAnimationFrame(this.startSVGRenderLoop);
        };


        worldPosFromObjectName(name: string) {
            const found = this[$scene].getObjectByName(name);

            if (found) {

                found.getWorldPosition(this.utilVec);
                // console.info("utilvec WORLDSPACE:")
                // console.info(this.utilVec.x, this.utilVec.y, this.utilVec.z)


                // convert to scene.target localspace 
                this[$scene].target.worldToLocal(this.utilVec);
                // console.info("utilvec scene.target LOCALSPACE:")
                // console.info(this.utilVec.x, this.utilVec.y, this.utilVec.z)


                // const result = this.utilVec.clone();


                return this.utilVec;


            } else {

                return null;

            }

        }

        addInstance(hook: any | undefined) {
            // Test: if complib, add first
            if (!hook) {
                return
            }
            if (Object.keys(this.componentLib).length) {
                const first = Object.keys(this.componentLib)[0];
                const firstClone = this.componentLib[first].clone();
                firstClone.visible = true;
                hook.object.add(firstClone);
            }
        }

        collectionsToObjects(collectionArray: Array<string>, currentModel: string) {
            const objectSet = new Set()

            //@ts-ignore
            collectionArray.forEach(cName => {
                //@ts-ignore
                if (cName in this.collections[currentModel]) {

                    //@ts-ignore
                    this.collections[currentModel][cName].forEach(o => {
                        objectSet.add(o)

                    })
                }
            })

            return [...objectSet];
        }

        /**
        * When fading, objects get a fading material assigned (transparent:true, opacity:#)
        * When fade is done, switch objects back to the original material.
        */
        resetFadeMaterials() {

            this.enableNodes.forEach(obj => {
                // Set visibility
                // Objects that have a fadingState of fadeOut
                // will get their visbility set to false.
                switch (this.objectNameToFadeMapping[obj.name]) {
                    case (EObjectFadeType.fadeOut):
                        obj.visible = false;
                        break;
                    default: null;
                }

                // Reset Material unless it is fadeCustom
                if (this.objectNameToFadeMapping[obj.name] !== EObjectFadeType.fadeCustom) {
                    this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeNone;

                    //@ts-ignore
                    if (obj.material) {
                        //@ts-ignore
                        obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeNone];
                    }
                }


                // Reset fadeIn/fadeOut opacities
                Object.keys(this.fadeMaterials).forEach((m) => {
                    //@ts-ignore
                    this.fadeMaterials[m].fadeIn.opacity = 0;
                    //@ts-ignore
                    this.fadeMaterials[m].fadeOut.opacity = 1;
                })
            })

        }


        handleVisibilities(sData: object, currentModel: string) {
            //@ts-ignore
            if (!this.collections || !this.collections[currentModel]) {
                return
            }

            // Do not fade if we are in the same shot!
            if (!this.newShot) {
                return
            }


            // We will process visibilities first, so objects can fade in and out.
            // If objects are set via fadeCollection, they will be handled 
            // right after, so they overwrite whatever the enabledHandler is doing.

            // handle object visibilities. 
            // This will sort objects by their fadeState (in, out, custom, none)
            this.enabledHandler(sData, currentModel);

            // handle fade
            // this.fadeHandler(sData, currentModel);


            // Start visibility counter. Will be used to drive fadeIn and Out
            this.fadeCounter = 0;
            this.fadeInProgress = true;

        }

        /**
         * Handles visibilities and fading objects
         * @param sData 
         * @param currentModel 
         * @returns 
         */
        enabledHandler(sData: object, currentModel: string) {
            //@ts-ignore
            if (!Array.isArray(sData.setEnabled) || sData.enabledState == null) {
                return
            }

            //@ts-ignore
            if (sData.fadeDuration != null) {
                //@ts-ignore
                this.fadeDuration = sData.fadeDuration;
            }

            // Convert list of collections to list of objects
            //@ts-ignore
            const listOfObjects = this.collectionsToObjects(sData.setEnabled, currentModel) as Array<string>;
            let fadeListOfObjects: Array<string> = []
            let filteredListOfObjects: Array<string> = [];

            // Remove objects in setEnabled list  that are in fadeCollection
            //@ts-ignore
            if (Array.isArray(sData.fadeCollections || sData.fadeAmount != null)) {
                //@ts-ignore
                fadeListOfObjects = this.collectionsToObjects(sData.fadeCollections, currentModel);
                filteredListOfObjects = listOfObjects.filter(x => !fadeListOfObjects.includes(x));
            } else {
                filteredListOfObjects = listOfObjects
            }

            this.enableNodes.forEach(obj => {
                //@ts-ignore
                switch (sData.enabledState) {
                    case true:
                        // Handle ALL objects in the scene.
                        if (filteredListOfObjects.includes(obj.name)) {
                            // Object needs to be visible
                            if (obj.visible) {
                                // do nothing / apply original material and keep visibility
                                this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeNone;
                                //@ts-ignore
                                if (obj.material) {
                                    //@ts-ignore
                                    obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeNone];
                                }
                                obj.visible = true;
                            } else {
                                this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeIn;
                                //@ts-ignore
                                if (obj.material) {
                                    // console.info("this.fadeMaterials[obj.material.name][EObjectFadeType.fadeIn]")
                                    //@ts-ignore
                                    // console.info(this.fadeMaterials[obj.material.name][EObjectFadeType.fadeIn])
                                    //@ts-ignore
                                    obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeIn];
                                }
                                // Set it visible now, so we can see the fadeIn
                                obj.visible = true;
                            }
                        } else {
                            // Object needs to be hidden
                            if (obj.visible) {
                                // fade out
                                this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeOut;
                                //@ts-ignore
                                if (obj.material) {
                                    //@ts-ignore
                                    obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeOut];
                                    // For objects that have no material, they will be set visible after the fade.
                                }

                                // Visibility will be set after fade is finished!

                            } else {
                                // do nothing / apply original material and keep invisible
                                this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeNone;
                                //@ts-ignore
                                if (obj.material) {
                                    //@ts-ignore
                                    obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeNone];
                                }
                                obj.visible = false;
                            }
                        }
                        break;


                    case false:
                        // hide objects specified
                        if (filteredListOfObjects.includes(obj.name)) {
                            // if visible
                            if (obj.visible) {
                                // fadeOut
                                this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeOut;
                                //@ts-ignore
                                if (obj.material) {
                                    //@ts-ignore
                                    obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeOut];
                                }
                            } else {
                                // do nothing / apply original material and keep invisible
                                this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeNone;
                                //@ts-ignore
                                if (obj.material) {
                                    //@ts-ignore
                                    obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeNone];
                                }
                                obj.visible = false;
                            }
                            // else do nothing
                            obj.visible = false;
                        } else {
                            if (obj.visible) {
                                // do nothing / apply original material and keep visibility
                                this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeNone;
                                //@ts-ignore
                                if (obj.material) {
                                    //@ts-ignore
                                    obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeNone];
                                }
                                obj.visible = true;
                            } else {
                                this.objectNameToFadeMapping[obj.name] = EObjectFadeType.fadeIn;
                                //@ts-ignore
                                if (obj.material) {
                                    // console.info("this.fadeMaterials[obj.material.name][EObjectFadeType.fadeIn]")
                                    //@ts-ignore
                                    // console.info(this.fadeMaterials[obj.material.name][EObjectFadeType.fadeIn])
                                    //@ts-ignore
                                    obj.material = this.fadeMaterials[obj.material.name][EObjectFadeType.fadeIn];
                                }
                                // Set it visible now, so we can see the fadeIn
                                obj.visible = true;
                            }
                        }
                        break;
                    default: null;
                }
            })

            // Set fadeCustom
            fadeListOfObjects.forEach(fadeObjName => {
                // find object,
                const found = this[$scene].getObjectByName(fadeObjName);
                if (!found) {
                    return
                }

                this.objectNameToFadeMapping[fadeObjName] = EObjectFadeType.fadeCustom;

                //@ts-ignore
                this.fadeMaterials[found.material.name].fadeCustomAmount = sData.fadeAmount

                //@ts-ignore
                found.material = this.fadeMaterials[found.material.name][EObjectFadeType.fadeCustom];

                found.visible = true;
            })
        };

        degreesToRadians(degrees: number) {
            const pi = Math.PI;
            return degrees * (pi / 180);
        }

        radiansToDegrees(radians: number) {
            return radians * 180 / Math.PI;
        }


        easing(type = "easeInOutSine", x: number): number {
            let c4;
            switch (type) {
                case ("easeOutCirc"):
                    return Math.sqrt(1 - Math.pow(x - 1, 2));
                    break;
                case ("easeOutElastic"):
                    c4 = (2 * Math.PI) / 3;
                    return x === 0
                        ? 0
                        : x === 1
                            ? 1
                            : Math.pow(2, -10 * x) * Math.sin((x * 10 - 0.75) * c4) + 1;
                    break;
                default:
                    //easeInOutSine
                    return -(Math.cos(Math.PI * x) - 1) / 2;
            }
        }

        handleExposure(targetExposure: number, duration: number) {

            if (!this.newShot) {
                return
            }

            this.ezTweenStore[ETween.exposure] = {
                valueStart: this[$scene].exposure,
                valueEnd: targetExposure,
                // valueDelta: targetExposure - this.scene.exposure,
                duration: duration,
                progress: 0,
                ease: 'easeInOutSine'
            };

        }

        envHdr(p: string = "d/cubemaps/default.hdr") {
            if (this.assetsPath) {
                //@ts-ignore
                this.environmentImage = this.assetsPath + p;
            } else {
                //@ts-ignore
                this.environmentImage = p;
            }
        }

        lerp(a: number, b: number, alpha: number) {

            const num = a + alpha * (b - a)
            // round to 5 decimal points            
            return Math.round(num * 10000) / 10000;
        }


        materialTweaks(materialTweaksArray: Array<object>) {

            materialTweaksArray.forEach((matTweaksObj) => {
                //@ts-ignore
                let material = [...this.materials].find(o => o.name === matTweaksObj.materialName);

                if (!material) return;
                //@ts-ignore
                if (matTweaksObj.toneMapped != null) {
                    //@ts-ignore
                    material.toneMapped = matTweaksObj.toneMapped;
                };

                //@ts-ignore
                if (matTweaksObj.flatShading != null) {
                    //@ts-ignore
                    material.flatShading = matTweaksObj.flatShading;
                };

                //@ts-ignore
                if (matTweaksObj.emissiveIntensity != null) {
                    //@ts-ignore
                    material.emissiveIntensity = matTweaksObj.emissiveIntensity;
                };

                //@ts-ignore
                if (matTweaksObj.depthTest != null) {
                    //@ts-ignore
                    material.depthTest = matTweaksObj.depthTest;
                };

                //@ts-ignore
                if (matTweaksObj.transparent != null) {
                    //@ts-ignore
                    material.transparent = matTweaksObj.transparent;
                };

                //@ts-ignore
                if (matTweaksObj.opacity != null) {
                    //@ts-ignore
                    material.opacity = matTweaksObj.opacity;
                };

                //@ts-ignore
                if (matTweaksObj.depthWrite != null) {
                    //@ts-ignore
                    material.depthWrite = matTweaksObj.depthWrite;
                };

                //@ts-ignore
                if (matTweaksObj.side) {
                    //@ts-ignore
                    switch (matTweaksObj.side) {
                        case "front":
                            //@ts-ignore    
                            material.side = FrontSide;
                            break;
                        case "back":
                            //@ts-ignore    
                            material.side = BackSide;
                            break;
                        case "double":
                            //@ts-ignore    
                            material.side = DoubleSide;
                            break;
                        default:
                            null;

                    }

                };

                //@ts-ignore
                if (matTweaksObj.blending) {
                    //@ts-ignore
                    switch (matTweaksObj.blending) {

                        case "noBlending":
                            //@ts-ignore    
                            material.blending = NoBlending;
                            break;
                        case "normalBlending":
                            //@ts-ignore    
                            material.blending = NormalBlending;
                            break;
                        case "additiveBlending":
                            //@ts-ignore    
                            material.blending = AdditiveBlending;
                            break;
                        case "subtractiveBlending":
                            //@ts-ignore    
                            material.blending = SubtractiveBlending;
                            break;
                        case "multiplyBlending":
                            //@ts-ignore    
                            material.blending = MultiplyBlending;
                            break;
                        default:
                            null;

                    }

                };
            })
        }

        uvOffset(uvOffsetsArray: Array<object>) {

            uvOffsetsArray.forEach(uvOffsetObject => {

                //@ts-ignore
                let material: Material = [...this.materials].find(o => o.name === uvOffsetObject.materialName);

                if (!material) {
                    return;
                }


                // Find sourceNode in scene
                this[$scene].traverse((n) => {

                    //@ts-ignore
                    if (n.name === uvOffsetObject.sourceNode) {

                        // connect X,Y -> U,V
                        //@ts-ignore
                        if (material.map) {
                            //@ts-ignore
                            material.map.offset =
                                n.position;
                        }


                        // Add material to updateMaterials if
                        // update is needed in update loop
                        //@ts-ignore
                        if (uvOffsetObject.animateOpacity) {
                            //@ts-ignore
                            this.uvOffsetMaterials[material.name] = {
                                material: material,
                                sourceVec: n.position
                            };
                        }


                    }
                });

            })


        }


        async handleHotspots(sData: object) {
            if (!this.ucxMeshMapping) {
                if (this.dev) {
                    console.info("No ucxMeshMapping");
                }
                return;
            }

            this.removeAllChildNodes();

            //  <div slot="hotspot-hoof" class="anchor" data-surface="0 0 752 733 735 0.132 0.379 0.489"></div>

            // construct data-surface
            //@ts-ignore
            if (!sData?.hotspots) {
                return
            }

            // hotspots are array of objects with 
            interface IHotspot {
                target: string, // ucxnode
                contentId: string // element id in DOM to use as hotspot
            }

            //@ts-ignore
            const hotspots: Array<IHotspot> = sData.hotspots;

            for (let hspot of hotspots) {

                if (this.dev) {
                    // Add a marker 
                    const hDiv = document.createElement('div');
                    hDiv.innerText = hspot.target + " - " + hspot.contentId;
                    //@ts-ignore
                    const dataSurface = this.ucxMeshMapping[this.currentModel][hspot.target] + " 0 0 1 2 1 1 1"
                    hDiv.slot = `hotspot-${hspot.target}`;

                    hDiv.dataset.surface = `${dataSurface}`

                    this.appendChild(hDiv);
                }

                // find elemnt for hotspot in DOM
                const foundEl = document.querySelector(`#${hspot.contentId}`);



                if (!foundEl) { continue };


                //@ts-ignore
                if (!this.ucxMeshMapping[this.currentModel] && !this.ucxMeshMapping[this.currentModel][hspot.target]) {
                    continue
                };

                //@ts-ignore
                const dataSurface = this.ucxMeshMapping[this.currentModel][hspot.target] + " 0 0 1 2 1 1 1"
                if (this.dev) {
                    console.info("Hotspots ", hspot.target, dataSurface);
                }


                const clonedEl: HTMLElement = foundEl.cloneNode(true) as HTMLElement;

                clonedEl.slot = `hotspot-${hspot.target}`;

                clonedEl.dataset.surface = `${dataSurface}`

                this.appendChild(clonedEl);

                // TODO implement UCXTARGET ( see below )

            };

            /*
            //@ts-ignore
            Object.keys(sData.hotspots).forEach(hName => {
                //ucxMeshMapping[hName];

                const hDiv = document.createElement('div');
                hDiv.classList.add('anchor', 'h_container');
                hDiv.slot = `hotspot-${hName}`;
                //data-visibility-attribute="visible"
                hDiv.setAttribute("data-visibility-attribute", "visible");
                hDiv.dataset.surface = `${dataSurface}`
                //@ts-ignore
                hDiv.innerHTML = sData.hotspots[hName].content;

                //@ts-ignore
                if (sData?.hotspots[hName]?.classes) {
                    //@ts-ignore
                    hDiv.classList.add(...sData.hotspots[hName].classes);
                }

                const targetName = hName.replace("UCX_", "UCXTARGET_");
                //@ts-ignore
                if (this.ucxMeshMapping[this.currentModel][targetName]) {
                    if (this.dev) {
                        console.info("Hotspot has target ", targetName);
                    }

                    // Add SVG Container if not exists:
                    let svgContainer = this.querySelector('#hotspotLineContainer');
                    if (svgContainer !== null && svgContainer.nodeName.toLowerCase() === "svg") {
                        if (this.dev) {
                            console.info("Has svg container");
                        }
                    } else {
                        if (this.dev) {
                            console.info("Adding new svg container")
                        }
                        // create new svg container
                        // const svgEl = document.createElement('svg');
                        const svgEl = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
                        svgEl.id = "hotspotLineContainer";
                        svgEl.setAttribute("width", "100%");
                        svgEl.setAttribute("height", "100%");
                        // svgEl.setAttribute("xmlns", "http://www.w3.org/2000/svg");
                        // svgEl.classList.add("hotspotLineContainer");
                        this.appendChild(svgEl);
                        svgContainer = svgEl;

                    }

                    // Add UCXTARGET as hotspot div
                    //@ts-ignore
                    const dataSurface = this.ucxMeshMapping[this.currentModel][targetName] + " 0 0 1 2 1 1 1"
                    const hTargetDiv = document.createElement('div');
                    // hTargetDiv.classList.add('anchor', 'h_container');
                    hTargetDiv.slot = `hotspot-${targetName}`
                    hTargetDiv.dataset.surface = `${dataSurface}`
                    // hDiv.innerHTML = sData.hotspots[hName].content;
                    this.appendChild(hTargetDiv);

                    // Add line Element to SVG Container
                    //<line class="line"></line>
                    // const lineEl = document.createElement('line');
                    const lineEl = document.createElementNS(
                        'http://www.w3.org/2000/svg',
                        'line'
                    );
                    lineEl.dataset.start = hName; // name of start
                    lineEl.dataset.end = targetName;
                    lineEl.classList.add("hotspotLine");
                    svgContainer?.appendChild(lineEl);
                    this.lineRenderElements.push(lineEl);

                } else { if (this.dev) { console.info("Has no target") } }

                //@ts-ignore
                if (sData?.hotspots[hName]?.expanded) {
                    hDiv.classList.add("expanded");
                }

                this.appendChild(hDiv);
                this.startSVGRenderLoop();
            })
            */
        }


        [$tick](time: number, delta: number) {
            super[$tick](time, delta);

            delta;
            // TODO We should only update this, if objects with material are visible
            if (Object.keys(this.uvOffsetMaterials).length) {
                Object.keys(this.uvOffsetMaterials).forEach(m => {
                    //@ts-ignore
                    this.uvOffsetMaterials[m].material.opacity = this.uvOffsetMaterials[m].sourceVec.z
                })
            }

            // Update fadeCounter and fadeMaterial opacities

            if (this.fadeInProgress) {
                this.fadeCounter = this.fadeCounter + (1 / this.fadeDuration);
                if (this.fadeCounter >= 1) {
                    this.fadeInProgress = false;
                    this.fadeCounter = 0;
                    this.resetFadeMaterials();
                    return;
                }
                Object.keys(this.fadeMaterials).forEach((m) => {
                    if (0 + this.fadeCounter >= 1 || 1 - this.fadeCounter <= 0) {
                        return
                    }
                    //@ts-ignore
                    this.fadeMaterials[m].fadeIn.opacity = 0 + this.fadeCounter;
                    //@ts-ignore
                    this.fadeMaterials[m].fadeOut.opacity = 1 - this.fadeCounter;

                    //@ts-ignore
                    if (0 + this.fadeCounter < this.fadeMaterials[m].fadeCustomAmount) {
                        this.fadeMaterials[m].fadeCustom.opacity = 0 + this.fadeCounter;
                    }
                })
            }


            // Update Tweens
            Object.keys(this.ezTweenStore).forEach(s => {

                const tweenType: ETween = s as ETween;

                const tween = this.ezTweenStore[tweenType];

                if (!tween) { return };

                // Remove finished Tweens
                if (tween.progress >= 1) {

                    delete this.ezTweenStore[tweenType];
                }

                tween.progress = tween.progress + (1 / tween.duration);


                switch (tweenType) {
                    case 'exposure':
                        this[$scene].exposure = this.lerp(tween.valueStart, tween.valueEnd, this.easing("easeInOutSine", tween.progress));
                        break
                    default:
                        null
                }
            })

            // Update animation weights
            this.animHandler.update(delta);
        }
    }

    return EchtzeitModelViewerElement

}