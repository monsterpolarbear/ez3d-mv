#!/bin/bash
branch_name=$(git symbolic-ref -q HEAD)
branch_name=${branch_name##refs/heads/}
branch_name=${branch_name:-HEAD}
echo $branch_name

basepath=~/dev/ezViewer/static/mv
output=$basepath/$branch_name

# if branch name is 'master', copy into static/mv

if [ "$branch_name" = "master" ];
then
 output=$basepath
fi

echo output is $output

if [ "$1" != "dev" ]; # in development mode
then
 echo Production
 rm -f dist/model-viewer.min.js.map
 rm -rf $basepath/*
 cp -a dist/. $output
else
 echo dev
 rm -rf $basepath/*
 cp -a dist/. $output/
 cp -a dist/model-viewer.min.js.map $basepath/
 touch $basepath/dev
fi



